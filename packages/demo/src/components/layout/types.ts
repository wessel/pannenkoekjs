export interface MenuItem {
  label: string;
  url: string;
  active?: boolean;
  match?: string;
}
