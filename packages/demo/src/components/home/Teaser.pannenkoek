<script>
  import Code from '../code/Code.pannenkoek';

  export default class {
    public title!: string;
    public code!: string;
    public description?: string;
  }
</script>

<section class="teaser">
  <div class="description">
    <h1>{{ title }}</h1>
    {% if description %}
    <h2>{{ description }}</h2>
    {% endif %}
  </div>

  <Code>{% slot %}{% endslot %}</Code>
</section>

<style>
  .teaser {
    display: grid;
    grid-template-columns: 1fr 1fr;
    column-gap: 12px;
    margin: 0;
    padding: 48px 0;

    &~& {
      border-top: 1px solid #eee;
    }

    .description {
      h1 {
        margin: 0 0 12px;
        font-size: 2rem;
      }

      h2 {
        margin: 0;
        font-size: 1rem;
        font-weight: normal;
      }
    }

    @media only screen and (max-width: 1024px) {
      grid-template-columns: auto;
      grid-auto-flow: row;
      row-gap: 24px;
    }
  }
</style>