<script>
  import { SourceFile } from '../../types';
  import Code from './Code.pannenkoek';
  import fileIcon from './img/icon.svg';

  export default class {
    public files!: SourceFile[];
    private activeFile!: SourceFile;
    private mode: 'source' | 'ts' | 'style' = 'source';

    public onMount() {
      // this hack is needed because PannenkoekJS doesn't reload the Example on hash change
      window.addEventListener('hashchange', this.resetActiveItem);

      this.resetActiveItem();
    }

    public onRemove(): void {
      window.removeEventListener('hashchange', this.resetActiveItem);
    }

    private resetActiveItem() {
      this.activeFile = this.files[0];
      this.mode = 'source';
    }

    private setActiveFile(file: SourceFile) {
      this.mode = 'source';
      this.activeFile = file;
    }

    private getCodeContent() {
      if (!this.activeFile) {
        return '';
      }

      switch (this.mode) {
        case 'source':
          return this.activeFile.source;

        case 'ts':
          return this.activeFile.compiled;

        case 'style':
          return this.activeFile.style;
      }
    }
  }
</script>

<div class="container">
  <ul class="tabs">
    {% each file in files %}
    <li :class="'tab-item' + (activeFile && activeFile.name === file.name ? ' active' : '')"
      @click="setActiveFile(file)">
      <img :src="fileIcon" />
      <span>{{ file.name }}</span>
    </li>
    {% endeach %}
  </ul>

  <div class="code-container">
    <div class="code-actions">
      <div @click="mode = 'source'" :class="'action' + (mode === 'source' ? ' active' : '')">Source</div>

      {% if activeFile && activeFile.compiled %}
      <div @click="mode = 'ts'" :class="'action' + (mode === 'ts' ? ' active' : '')">Generated TS</div>
      {% endif %}

      {% if activeFile && activeFile.scss %}
      <div @click="mode = 'style'" :class="'action' + (mode === 'style' ? ' active' : '')">Generated SCSS</div>
      {% endif %}
    </div>

    <Code>
      {{ getCodeContent() }}
    </Code>
  </div>
</div>


<style>
  .container {
    position: relative;
    overflow: hidden;
  }

  .tabs {
    margin: 0;
    padding: 0;
    list-style: none;
    display: flex;
    width: 100%;
    overflow-x: auto;
  }

  .tab-item {
    margin: 0;
    background-color: #555;
    color: #eee;
    padding: 12px;
    display: inline-block;
    cursor: pointer;
    border-right: 1px solid #333;
    white-space: nowrap;

    img {
      width: 18px;
      vertical-align: middle;
      margin-right: 0.3rem;
    }

    span {
      vertical-align: middle;
    }

    &.active {
      background-color: #333;
    }

    &:last-of-type {
      border-right: 0;
    }
  }

  .code-container {
    position: relative;
    background-color: #333;

    .code-actions {
      padding: 10px 10px 0;
      text-align: right;

      >button {
        margin: 0 5px;
      }

      .action {
        display: inline-block;
        color: #eee;
        padding: 5px;
        border: 1px solid #eee;
        cursor: pointer;
        margin-left: -1px;

        &.active {
          background: #ccc;
          border-color: #ccc;
          color: #333;
        }
      }
    }
  }
</style>