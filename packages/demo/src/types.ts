export interface SourceFile {
  name: string;
  source: string;
  compiled?: string;
  style?: string;
}

export interface ExampleGroup {
  id: string;
  title: string;
  examples: ExampleItem[];
}

export interface ExampleItem {
  id: string;
  title: string;
  sourceFiles: SourceFile[];
  component: string;
}
