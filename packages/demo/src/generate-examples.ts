import { Compiler, Parser } from '@pannenkoekjs/compiler/src/main';
import { paramCase, sentenceCase } from 'change-case';
import { lstatSync, readdirSync, readFileSync, writeFileSync } from 'fs';
import { basename, resolve } from 'path';
import { ExampleGroup, ExampleItem, SourceFile } from './types';

const examplesDir = resolve(__dirname, 'components', 'examples');

generateExampleCode();

function generateExamples(): [ExampleGroup[], string[]] {
  const groups = readdirSync(examplesDir)
    .filter((name) => lstatSync(resolve(examplesDir, name)).isDirectory());

  const imports: string[] = [];
  const examples: ExampleGroup[] = [];

  for (const groupName of groups) {
    const groupPath = resolve(examplesDir, groupName);
    const exampleNames = readdirSync(groupPath);

    const group: ExampleGroup = {
      id: removeLeadingNumber(groupName),
      title: sentenceCase(removeLeadingNumber(groupName)),
      examples: [],
    };

    for (const exampleName of exampleNames) {
      const filePaths = readdirSync(resolve(groupPath, exampleName))
        .map(filename => resolve(groupPath, exampleName, filename));

      const sanitizedName = removeLeadingNumber(exampleName);

      imports.push(`import ${sanitizedName} from './${groupName}/${exampleName}/App.pannenkoek';`);

      const example: ExampleItem = {
        id: paramCase(sanitizedName),
        title: sentenceCase(sanitizedName),
        component: sanitizedName,
        sourceFiles: [],
      };

      for (const path of filePaths) {
        const content = readFileSync(path).toString();

        example.sourceFiles.push(compilePannenkoek(path, content));
      }

      group.examples.push(example);
    }

    examples.push(group);
  }

  return [examples, imports];
}

async function generateExampleCode() {
  const [examples, imports] = generateExamples();

  const code = `
  // tslint:disable
  ${imports.join('\n')}

  export default ${JSON.stringify(examples, undefined, 2).replace(/"component": "(.*)"/g, '"component": $1')};
  `;

  writeFileSync(resolve(examplesDir, 'Generated.ts'), code.trim());

}

function removeLeadingNumber(name: string): string {
  return name.replace(/^\d+\. /, '');
}

function compilePannenkoek(path: string, source: string): SourceFile {
  const file: SourceFile = {
    source,
    name: basename(path),
  };

  if (!path.match(/\.pannenkoek$/)) {
    return file;
  }

  const name = basename(path).replace('.pannenkoek', '');

  const parser = new Parser();
  const compiler = new Compiler({
    shortPropertyNames: false,
  });

  const parsed = parser.parse(source, name);
  const compiled = compiler.compile(parsed);

  file.compiled = compiled.code;
  file.style = compiled.style;

  return file;
}
