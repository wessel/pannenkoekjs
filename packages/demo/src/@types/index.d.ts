declare module '*.svg';

declare module '*.pannenkoek' {
  import { Component } from '@pannenkoekjs/runtime';

  export default class ComponentInstance {
    public $$component: Component;
    constructor(...args: any[]);
  }
}
