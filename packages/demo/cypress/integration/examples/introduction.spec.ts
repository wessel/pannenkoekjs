/// <reference types="Cypress" />

context('Introduction', () => {
  it('Hello world', () => {
    cy.visit('http://localhost:8080/#/examples/hello-world');

    cy.contains('h1', 'Hello world');
    cy.contains('section h1', 'Output').parent().contains('Hello, world!');
  });

  it('Dynamic attributes', () => {
    cy.visit('http://localhost:8080/#/examples/dynamic-attributes');

    cy.contains('h1', 'Dynamic attributes');
    cy.contains('section h1', 'Output').parent()
      .find('img')
      .should('have.attr', 'src', 'https://svelte.dev/tutorial/image.gif')
      .should('have.attr', 'alt', 'Rick Astley dancing');
  });

  it('Styling', () => {
    cy.visit('http://localhost:8080/#/examples/styling');

    cy.contains('h1', 'Styling');

    cy.contains('section h1', 'Output').parent().contains('h1', 'Very styled title').should('have.attr', 'class');
    cy.contains('section h1', 'Output').parent().contains('p', 'All css is locally scoped by default!').should('have.attr', 'class');
  });

  it('Reactive assignments', () => {
    cy.visit('http://localhost:8080/#/examples/reactive-assignments');

    cy.contains('h1', 'Reactive assignments');
    cy
      .contains('section h1', 'Output')
      .parent()
      .contains('button', 'Clicked 0 times')
      .click()
      .contains('Clicked 1 times')
      .click()
      .contains('Clicked 2 times');
  });

  it('Nested', () => {
    cy.visit('http://localhost:8080/#/examples/nested');

    cy.contains('h1', 'Nested');
    cy
      .contains('section h1', 'Output')
      .parent()
      .contains('You can add nested components in the middle of a sentence. How neat is that \\\\o/');
  });

  it('Raw statements', () => {
    cy.visit('http://localhost:8080/#/examples/raw-statements');

    cy.contains('h1', 'Raw statements');
    cy
      .contains('section h1', 'Output')
      .parent()
      .contains(`
<script>
  export default class {
    private world = 'world';
  }
</script>

<p>Hello, {{ world }}!</p>

<style>
p {
  color: pink;
}
</style>
      `.trim());
  });

  it('Refs', () => {
    cy.visit('http://localhost:8080/#/examples/refs');

    cy.contains('h1', 'Refs');
    cy
      .contains('section h1', 'Output')
      .parent()
      .contains('Some text within a ref');
  });

});
