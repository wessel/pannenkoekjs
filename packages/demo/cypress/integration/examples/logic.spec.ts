/// <reference types="Cypress" />

context('Logic', () => {
  it('Hello world', () => {
    cy.visit('http://localhost:8080/#/examples/if-blocks');

    cy.contains('h1', 'If blocks');
    cy.contains('section h1', 'Output').parent();
  });
});
