import { mkdirSync, writeFileSync } from 'fs';
import { getOptions } from 'loader-utils';
import { dirname, resolve } from 'path';
import { loader } from 'webpack';

// tslint:disable-next-line: only-arrow-functions
const pannenkoekJsLoader: loader.Loader = function (source) {
  const path = getOptions(this).path || '/dist/generated-ts';

  const destinationDir = resolve(this.rootContext, path);
  const destination = destinationDir + this.resource.replace(`${this.rootContext}/src`, '').replace('.pannenkoek', '.pannenkoek.ts');

  mkdirSync(dirname(destination), { recursive: true });
  writeFileSync(destination, source);

  return source;
};

export default pannenkoekJsLoader;
