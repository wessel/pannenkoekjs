import { getOptions } from 'loader-utils';
import { loader } from 'webpack';

// tslint:disable-next-line: only-arrow-functions
const extractCssLoader: loader.Loader = function () {
  return getOptions(this).source;
};

export default extractCssLoader;
