import { Compiler, Parser } from '@pannenkoekjs/compiler';
import { CompilationResult } from '@pannenkoekjs/compiler/dist/compiler/CompilationResult';
import { CompilerOptions } from '@pannenkoekjs/compiler/dist/compiler/CompilerOptions';
import { getOptions, getRemainingRequest, stringifyRequest } from 'loader-utils';
import { loader } from 'webpack';

// tslint:disable-next-line: only-arrow-functions
const pannenkoekJsLoader: loader.Loader = function (source) {
  const compiled = compile(this.resource, source, getOptions(this));

  if (!compiled.style) {
    return compiled.code;
  }

  return addStylesImport(
    this.resource,
    compiled,
    getRemainingRequest(this),
    this.context as any,
  );
};

export default pannenkoekJsLoader;

function compile(
  filePath: string,
  source: string | Buffer,
  options: CompilerOptions,
): CompilationResult {
  const parser = new Parser();
  const compiler = new Compiler(options);

  const name = filePath.split('/').pop()!.replace('.pannenkoek', '').replace(/[^a-zA-Z]/g, '');
  const parsedTemplate = parser.parse(source.toString(), name);

  return compiler.compile(parsedTemplate);
}

function addStylesImport(
  filePath: string,
  compiled: CompilationResult,
  request: string,
  context: loader.LoaderContext,
): string {
  const sourceLoader = require.resolve('./source-loader');
  const opts = { source: compiled.style };
  const resource = `${filePath}.scss!=!${sourceLoader}?${JSON.stringify(opts)}!${request}`;

  return `
    import ${stringifyRequest(context, resource)};
    ${compiled.code}
  `;
}
