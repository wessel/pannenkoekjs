import { Component } from './component/Component';

type ObjectWithComponent = object & {
  $$component: Component,
};

export const createProxy = (component: ObjectWithComponent): any => {
  return new Proxy(component, {
    get(target, propertyKey, receiver) {
      const value = Reflect.get(target, propertyKey, receiver);

      if (typeof (propertyKey) === 'string' && propertyKey.startsWith('$$')) {
        return value;
      }

      if (!!value && typeof value === 'object') {
        return createProxy2(component, value);
      }

      return value;
    },

    set(target, propertyKey, value, receiver) {
      const previous = Reflect.get(target, propertyKey, receiver);

      if (previous !== value) {
        component.$$component.invalidate(propertyKey);
      }

      return Reflect.set(target, propertyKey, value, receiver);
    },
  });
};

const createProxy2 = (component: ObjectWithComponent, targetObject: object): any => {
  return new Proxy(targetObject, {
    get(target, propertyKey, receiver) {
      const value = Reflect.get(target, propertyKey, receiver);

      if (!!value && typeof value === 'object') {
        return createProxy2(component, value);
      }

      return value;
    },

    set(target, propertyKey, value, receiver) {
      const previous = Reflect.get(target, propertyKey, receiver);

      if (previous !== value) {
        component.$$component.invalidate(propertyKey);
      }

      return Reflect.set(target, propertyKey, value, receiver);
    },
  });
};
