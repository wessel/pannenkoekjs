import { Block } from '../component/Block';
import { ComponentClass } from '../component/Component';
import { Slots } from '../component/Slots';

export class ComponentElementWrapper {
  public anchor: Text;
  public instance?: ComponentClass;
  private slotTargets: Slots;

  constructor(
    private getContext: () => any,
    private slots: Slots,
    private construct: () => new (props: any) => ComponentClass,
    private childrenConstruct?: new () => Block,
  ) {
    this.anchor = document.createTextNode('');
    this.slotTargets = this.createSlotTargets();
  }

  public update(ctx: any, props: any) {
    this.evaluate(props);

    if (this.slotTargets.default && this.slotTargets.default.instance) {
      this.slotTargets.default.instance.update(ctx);
    }
  }

  public evaluate(props: any) {
    const construct = this.construct();

    if (!construct) {
      if (this.instance) {
        this.instance.$$component.remove();
        this.instance = undefined;
      }

      return;
    }

    if (this.instance instanceof construct) {
      return;
    }

    if (this.instance) {
      this.instance.$$component.remove();

      if (this.slotTargets.default && this.slotTargets.default.instance) {
        this.slotTargets.default.instance.remove();
        delete this.slotTargets.default.instance;
      }
    }

    this.instance = new construct(props);

    const target = document.createDocumentFragment();
    this.instance.$$component.mount(target, this.slotTargets);
    this.anchor.parentNode!.insertBefore(target, this.anchor);
  }

  public remove() {
    if (this.slotTargets.default && this.slotTargets.default.instance) {
      this.slotTargets.default.instance.remove();
    }

    this.instance!.$$component.remove();
    this.anchor.remove();
  }

  private createSlotTargets(): Slots {
    if (!this.childrenConstruct) {
      return {};
    }

    return {
      default: {
        create: (target: any) => {
          const block = new this.childrenConstruct!();
          block.mount(target, this.slots, this.getContext());

          return block;
        },
      },
    };
  }
}
