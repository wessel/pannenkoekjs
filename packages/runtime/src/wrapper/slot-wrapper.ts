import { Block } from '../component/Block';
import { Slots } from '../component/Slots';

export class SlotWrapper {
  public anchor: Text;

  constructor(
    private name: string,
    private slots: Slots,
    private getContext: () => any,
    private blockConstruct?: new () => Block,
  ) {
    this.anchor = document.createTextNode('');
  }

  public mount() {
    this.createSlot();

    const slot = this.slots[this.name];

    if (!slot) {
      return;
    }

    const fragment = document.createDocumentFragment();
    slot.instance = slot.create(fragment);
    this.anchor.parentNode!.insertBefore(fragment, this.anchor);
  }

  public remove() {
    this.anchor.remove();

    const slot = this.slots[this.name];

    if (!slot || !slot.instance) {
      return;
    }

    slot.instance.remove();
    delete slot.instance;
  }

  private createSlot() {
    if (!this.slots[this.name]) {
      if (!this.blockConstruct) {
        return;
      }

      this.slots[this.name] = {
        create: (target) => {
          const block = new this.blockConstruct!();
          block.mount(target, this.slots, this.getContext());

          return block;
        },
      };
    }
  }
}
