import { Block } from '../component/Block';
import { Slots } from '../component/Slots';

export class IfStatementWrapper {
  public anchor: Text;
  private instance?: Block;

  constructor(
    private slots: Slots,
    private getContext: () => any,
    private evaluateExpression: () => boolean,
    private thenBranch: new () => Block,
    private elseBranch?: new () => Block,
  ) {
    this.anchor = document.createTextNode('');
  }

  public evaluate() {
    const evaluated = this.evaluateExpression();

    if (evaluated) {
      this.createInstance(this.thenBranch, this.elseBranch);

      return;
    }

    if (this.elseBranch) {
      this.createInstance(this.elseBranch, this.thenBranch);

      return;
    }

    if (this.instance) {
      this.removeInstance();
    }
  }

  public remove() {
    this.anchor.remove();

    this.removeInstance();
  }

  private createInstance(
    thenBranch: new () => Block,
    elseBranch?: new () => Block,
  ) {
    if (elseBranch && this.instance && this.instance instanceof elseBranch) {
      this.removeInstance();
    }

    if (this.instance) {
      this.instance.update(this.getContext());

      return;
    }

    this.createAndMountInstance(thenBranch);
  }

  private createAndMountInstance(construct: new () => Block) {
    const fragment = document.createDocumentFragment();
    const instance = new construct();
    instance.mount(fragment, this.slots, this.getContext());
    this.anchor.parentNode!.insertBefore(fragment, this.anchor);

    this.instance = instance;
  }

  private removeInstance(): undefined {
    if (!this.instance) {
      return;
    }

    this.instance.remove();
    this.instance = undefined;
  }
}
