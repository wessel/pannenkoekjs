import { Block } from '../component/Block';

export class EachStatementWrapper {
  public anchor: Text;
  private children: Block[] = [];

  constructor(
    private initializer: string,
    private getContext: () => any,
    private childConstruct: new () => Block,
    private evaluateChildren: () => any[],
  ) {
    this.anchor = document.createTextNode('');
  }

  public evaluate() {
    const evaluated = this.evaluateChildren();

    const maxLength = Math.max(evaluated.length, this.children.length);
    const newBlocks: Block[] = [];

    for (let i = 0; i < maxLength; i++) {
      if (i >= evaluated.length) {
        this.children[i].remove();
        continue;
      }

      const childCtx = {
        ...this.getContext(),
        [this.initializer]: evaluated[i],
      };

      if (i < this.children.length) {
        this.children[i].update(childCtx);
        newBlocks.push(this.children[i]);
        continue;
      }

      const block = new this.childConstruct();
      const fragment = document.createDocumentFragment();
      block.mount(fragment, {}, childCtx);
      this.anchor.parentNode!.insertBefore(fragment, this.anchor);
      newBlocks.push(block);
    }

    this.children = newBlocks;
  }

  public remove() {
    for (const child of this.children) {
      child.remove();
    }

    this.anchor.remove();
  }
}
