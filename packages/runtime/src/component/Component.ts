import { Block } from './Block';
import { Slots } from './Slots';
import { scheduleUpdate } from './update-schedule';

export type ComponentClass = object & {
  $$component: Component,
};

export class Component {
  public refs: Record<string, Node> = {};
  protected changed: Record<PropertyKey, boolean> = {};
  private mainBlock?: Block;
  private dirty = false;

  constructor(
    private original: any,
    private mainBlockConstruct: new () => Block,
  ) { }

  public invalidate(propertyKey: PropertyKey): void {
    debugLog(this.original, 'invalidate');

    this.changed[propertyKey as any] = true;
    this.dirty = true;
    scheduleUpdate(this);
  }

  public mount(target: Node, slots: Slots): void {
    debugLog(this.original, 'mount');

    if (this.mainBlock) {
      return;
    }

    this.mainBlock = new this.mainBlockConstruct();
    this.mainBlock.mount(target, slots, this.original);

    if (this.original.onMount) {
      this.original.onMount();
    }
  }

  public update(): void {
    if (!this.mainBlock || !this.dirty) {
      return;
    }

    debugLog(this.original, 'update');

    this.mainBlock.update(this.original);

    if (this.original.onUpdate) {
      this.original.onUpdate(this.changed);
    }

    this.dirty = false;
    this.changed = {};
  }

  public remove(): void {
    debugLog(this.original, 'remove');

    if (!this.mainBlock) {
      return;
    }

    if (this.original.onRemove) {
      this.original.onRemove();
    }

    this.mainBlock.remove();
  }
}

const debugLog = (instance: Component, label: string, ...optionalParams: any[]) => {
  if (process.env.NODE_ENV !== 'development') {
    return;
  }

  const name = Object.getPrototypeOf(instance).constructor.name;

  console.debug(`[${name}] ${label}`, ...optionalParams); // tslint:disable-line: no-console
};
