import { Block } from './Block';

export interface SlotTarget {
  create: (target: any) => Block;
  instance?: Block;
}

export type Slots = Record<string, SlotTarget>;
