import { Component } from './Component';

export const scheduleUpdate = (component: Component) => {
  schedule.add(component);
  resolvedPromise.then(updateComponents);
};

const updateComponents = () => {
  schedule.forEach(component => component.update());
  schedule.clear();
};

const schedule = new Set<Component>();
const resolvedPromise = new Promise(resolve => resolve());
