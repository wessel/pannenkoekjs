import { Slots } from './Slots';

export abstract class Block {
  public slots: Slots = {};
  public ctx: any = {};

  public mount(target: Node, slots: Slots, ctx: any): void {
    this.ctx = ctx;
    this.slots = slots;
  }

  public update(ctx: any): void {
    this.ctx = ctx;
  }

  public remove(): void {
    // empty
  }
}
