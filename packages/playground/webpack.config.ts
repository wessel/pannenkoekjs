import HtmlWebpackPlugin from 'html-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import OptimizeCSSAssetsPlugin from 'optimize-css-assets-webpack-plugin';
import { resolve } from 'path';
import TerserJSPlugin from 'terser-webpack-plugin';
import { Configuration } from 'webpack';

const mode = (process.env.NODE_ENV as 'production' | 'development' | 'none') || 'production';
const isProduction = mode === 'production';

const config: Configuration = {
  mode,
  entry: resolve(__dirname, 'src/main.ts'),
  stats: {
    children: false,
  },
  optimization: {
    minimize: isProduction,
    moduleIds: isProduction ? false : 'named',
    minimizer: [
      new TerserJSPlugin({ parallel: true }),
      new OptimizeCSSAssetsPlugin(),
    ],
  },
  module: {
    rules: [
      {
        test: /\.pannenkoek$/,
        loader: [
          {
            loader: 'ts-loader',
            options: {
              appendTsSuffixTo: [/\.pannenkoek$/],
            },
          },
          {
            loader: '@pannenkoekjs/webpack-loader',
            options: {
              shortPropertyNames: isProduction,
            },
          },
        ],
      },
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          'file-loader',
          'image-webpack-loader',
        ],
      },
      {
        test: /\.(s?)css$/i,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          'css-loader',
          'postcss-loader',
          'sass-loader',
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.pannenkoek', '.js', '.svg'],
    alias: {
      typescript: resolve(__dirname, '../../node_modules/typescript'),
    },
  },
  output: {
    path: resolve(__dirname, 'dist'),
    filename: 'bundle.[contenthash].js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'PannenkoekJS playground',
      template: 'src/index.html',
      minify: isProduction && {
        collapseWhitespace: true,
        removeComments: true,
        removeRedundantAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true,
        useShortDoctype: true,
      },
    }),

    new MiniCssExtractPlugin({
      filename: '[name].[contenthash].css',
    }),
  ],
};

export default {
  ...config,
  devServer: {
    contentBase: './dist',
  },
};
