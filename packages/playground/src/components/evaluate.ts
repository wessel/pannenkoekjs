import { CompilationResult } from '@pannenkoekjs/compiler/dist/compiler/CompilationResult';
import * as runtime from '@pannenkoekjs/runtime';
import { ModuleKind, ScriptTarget, transpile } from 'typescript';

export function evaluateComponent(compilation: CompilationResult) {
  if (!compilation) {
    return;
  }

  try {
    const transpiled = transpile(compilation.code, {
      target: ScriptTarget.ES2015,
      module: ModuleKind.CommonJS,
    });

    const code = `
      (function () {
        ${transpiled};

        return exports;
      })();
    `;

    return evaluate(code).default;
  } catch (err) {
    // tslint:disable-next-line: no-console
    console.error(err);
  }
}

function evaluate(code: string) {
  const require = (name: string) => {
    if (name === '@pannenkoekjs/runtime') {
      return runtime;
    }

    throw new Error(`Invalid import: ${name}`);
  };

  // tslint:disable-next-line: no-eval
  return eval(code);
}
