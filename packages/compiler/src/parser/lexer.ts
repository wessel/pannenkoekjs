import { states } from 'moo';

export default states({
  main: {
    SCRIPT_START: { match: '<script>', push: 'script_tag' },
    STYLE_START: { match: '<style>', push: 'style_tag' },
    RAW_START: { match: '{% raw %}', push: 'raw_tag' },
    TAG_START: { match: '<', push: 'tag' },
    EXPRESSION_START: { match: '{{', push: 'expression' },
    STATEMENT_START: { match: '{%', push: 'statement' },
    TEXT: { fallback: true },
  },

  script_tag: {
    SCRIPT_END: { match: '</script>', pop: 1 },
    TEXT: { fallback: true },
  },

  style_tag: {
    STYLE_END: { match: '</style>', pop: 1 },
    TEXT: { fallback: true },
  },

  raw_tag: {
    RAW_END: { match: '{% endraw %}', pop: 1 },
    TEXT: { fallback: true },
  },

  tag: {
    TAG_END: { match: '>', pop: 1 },
    SLASH: '/',
    EQUALS: '=',
    DOUBLE_COLON: ':',
    AT: '@',
    SCRIPT: 'script',
    STYLE: 'style',
    WS: { match: /[ \n]+/, lineBreaks: true },
    IDENTIFIER: /[\w\-]+/,
    DOUBLE_QUOTE: { match: '"', push: 'literal' },
    TEXT: { fallback: true },
  },

  literal: {
    DOUBLE_QUOTE: { match: '"', pop: 1 },
    TEXT: { fallback: true },
  },

  expression: {
    EXPRESSION_END: { match: '}}', pop: 1 },
    TEXT: { fallback: true },
  },

  statement: {
    STATEMENT_END: { match: '%}', pop: 1 },
    SLOT: 'slot',
    ENDSLOT: 'endslot',
    IF: { match: 'if', next: 'statement_if' },
    ELSE: 'else',
    ENDIF: 'endif',
    EACH: { match: 'each', push: 'statement_each' },
    ENDEACH: 'endeach',
    WS: { match: /[ \n]+/, lineBreaks: true },
    TEXT: { fallback: true },
  },

  statement_if: {
    STATEMENT_END: { match: '%}', pop: 1 },
    TEXT: { fallback: true },
  },

  statement_each: {
    IN: { match: ' in ', pop: 1 },
    LEFT_PAREN: '(',
    RIGHT_PAREN: ')',
    COMMA: ',',
    WS: { match: /[ \n]+/, lineBreaks: true },
    TEXT: { fallback: true },
  },
});
