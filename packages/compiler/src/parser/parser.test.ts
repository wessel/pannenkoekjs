import ComponentElement from '../ast/ComponentElement';
import EachStatement from '../ast/EachStatement';
import { ExpressionAttribute, ListenerAttribute, LiteralAttribute } from '../ast/element-attributes';
import HtmlElement from '../ast/HtmlElement';
import IfStatement from '../ast/IfStatement';
import PrintExpression from '../ast/PrintExpression';
import ScriptTag from '../ast/ScriptTag';
import Slot from '../ast/Slot';
import StyleTag from '../ast/StyleTag';
import Template from '../ast/Template';
import Text from '../ast/Text';
import Parser from './parser';

describe('parser', () => {
  let parser: Parser;

  beforeEach(() => {
    parser = new Parser();
  });

  it('should parse text', () => {
    const result = parser.parse('lorem ipsum...');

    expect(result).toEqual(new Template('NamelessComponent', [new Text('lorem ipsum...')]));
  });

  it('should sanitize whitespace in text', () => {
    const result = parser.parse('   \n\tlorem\t\n  ipsum...  \n\n\n \t');

    expect(result).toEqual(new Template('NamelessComponent', [new Text(' lorem ipsum... ')]));
  });

  it('should ignore whitespace only text nodes', () => {
    const result = parser.parse('  <div>  </div>  ');

    expect(result).toEqual(new Template('NamelessComponent', [new HtmlElement('div')]));
  });

  it('should parse html elements', () => {
    const result = parser.parse('<something></something>');

    expect(result).toEqual(new Template('NamelessComponent', [new HtmlElement('something')]));
  });

  it('should parse component elements', () => {
    const result = parser.parse('<Something></Something>');

    expect(result).toEqual(new Template('NamelessComponent', [new ComponentElement('Something')]));
  });

  it('should parse selfclosing html elements', () => {
    const result = parser.parse('<something/>');

    expect(result).toEqual(new Template('NamelessComponent', [new HtmlElement('something')]));
  });

  it('should parse selfclosing component elements', () => {
    const result = parser.parse('<Something/>');

    expect(result).toEqual(new Template('NamelessComponent', [new ComponentElement('Something')]));
  });

  it('should parse nested html elements', () => {
    const result = parser.parse('<root><outer><inner/></outer></root>');

    expect(result).toEqual(new Template('NamelessComponent', [
      new HtmlElement(
        'root',
        [],
        [new HtmlElement('outer', [], [new HtmlElement('inner')])],
      ),
    ]));
  });

  it('should parse element attributes', () => {
    const result = parser.parse(
      '<something literal="value" :expression="expr" @listener="listen"/>',
    );

    expect(result).toEqual(new Template('NamelessComponent', [
      new HtmlElement('something', [
        new LiteralAttribute('literal', 'value'),
        new ExpressionAttribute('expression', 'expr'),
        new ListenerAttribute('listener', 'listen'),
      ]),
    ]));
  });

  it('should parse script tags', () => {
    const result = parser.parse('<script>console.log("HUH"); Record<what, ever> </script>');

    expect(result).toEqual(new Template(
      'NamelessComponent',
      [],
      new ScriptTag('console.log("HUH"); Record<what, ever> '),
    ));
  });

  it('should parse style tags', () => {
    const result = parser.parse('<style>body {}</style>');

    expect(result).toEqual(new Template(
      'NamelessComponent',
      [],
      undefined,
      new StyleTag('body {}'),
    ));
  });

  it('should parse raw tags', () => {
    const result = parser.parse('{% raw %}<script>whatever</script>{% endraw %}');

    expect(result).toEqual(new Template(
      'NamelessComponent',
      [new Text('<script>whatever</script>')],
    ));
  });

  it('should parse slots', () => {
    const result = parser.parse('{% slot %}{% endslot %}');

    expect(result).toEqual(new Template('NamelessComponent', [new Slot('default')]));
  });

  it('should parse named slots', () => {
    const result = parser.parse('{% slot something %}{% endslot %}');

    expect(result).toEqual(new Template('NamelessComponent', [new Slot('something')]));
  });

  it('should parse slots w/default children', () => {
    const result = parser.parse('{% slot %}something{% endslot %}');

    expect(result).toEqual(new Template('NamelessComponent', [new Slot('default', [new Text('something')])]));
  });

  it('should parse print expressions', () => {
    const result = parser.parse('{{ print something }}');

    expect(result).toEqual(new Template('NamelessComponent', [
      new PrintExpression('print something'),
    ]));
  });

  it('should parse each statements #1', () => {
    const result = parser.parse('{% each item in list %}{% endeach %}');

    expect(result).toEqual(new Template('NamelessComponent', [
      new EachStatement('item', 'list', []),
    ]));
  });

  it('should parse each statements #2', () => {
    const result = parser.parse('{% each (item, index) in list %}{% endeach %}');

    expect(result).toEqual(new Template('NamelessComponent', [
      new EachStatement('item', 'list', [], 'index'),
    ]));
  });

  it('should parse each statements with children', () => {
    const result = parser.parse('{% each item in list %}<div/>something{% endeach %}');

    expect(result).toEqual(new Template('NamelessComponent', [
      new EachStatement(
        'item',
        'list',
        [
          new HtmlElement('div'),
          new Text('something'),
        ],
      ),
    ]));
  });

  it('should parse if statements', () => {
    const result = parser.parse('{% if some_condition %}blaat{% endif %}');

    expect(result).toEqual(new Template('NamelessComponent', [
      new IfStatement(
        'some_condition',
        [new Text('blaat')],
      ),
    ]));
  });

  it('should parse if statements with else branches', () => {
    const result = parser.parse('{% if some_condition %}blaat{% else %}something else{% endif %}');

    expect(result).toEqual(new Template('NamelessComponent', [
      new IfStatement(
        'some_condition',
        [new Text('blaat')],
        [new Text('something else')],
      ),
    ]));
  });

  it('should throw errors on syntax error', () => {
    const expectedMessage = [
      'Syntax error: Unexpected \"%}\" (T_STATEMENT_END) at line 1 col 9:',
      '',
      '  asdf {% %}',
      '          ^',
    ].join('\n');

    expect(() => parser.parse('asdf {% %}')).toThrowError(new SyntaxError(expectedMessage));
  });

  it('should throw errors on no results on finish', () => {
    const expectedMessage = [
      'Syntax error: Unexpected \"{%\" (T_STATEMENT_START) at line 1 col 1:',
      '',
      '  {%',
      '  ^',
    ].join('\n');

    expect(() => parser.parse('{%')).toThrowError(new SyntaxError(expectedMessage));
  });
});
