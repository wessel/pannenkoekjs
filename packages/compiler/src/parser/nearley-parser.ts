import { Token } from 'moo';
import { Grammar, Parser, ParserOptions } from 'nearley';
import TemplateNode from '../ast/TemplateNode';
import { SyntaxError } from './SyntaxError';

export default class NearleyParser extends Parser {
  private lastToken?: Token;

  constructor(gram: Grammar, options?: ParserOptions) {
    super(gram, options);

    const oldNext = this.lexer.next.bind(this.lexer);
    this.lexer.next = () => {
      const token = oldNext();

      if (token) {
        this.lastToken = token as Token;
      }

      return token;
    };
  }

  public finishNodes(): TemplateNode[] {
    const results = this.finish();

    if (results.length === 0) {
      if (!this.lastToken) {
        throw new Error('Got empty results and no last token');
      }

      this.reportError(this.lastToken);
      return [];
    }

    if (results.length > 1) {
      throw new Error('Did not expect to see more than 1 result');
    }

    return results[0];
  }

  public reportError(token: Token) {
    const message = this.lexer.formatError(
      token,
      `Syntax error: Unexpected ${JSON.stringify(token.value)} (T_${token.type})`,
    );

    throw new SyntaxError(message);
  }
}
