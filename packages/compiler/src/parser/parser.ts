import ScriptTag from '../ast/ScriptTag';
import StyleTag from '../ast/StyleTag';
import Template from '../ast/Template';
import grammar from './grammar';
import NearleyParser from './nearley-parser';

export default class Parser {
  public parse(code: string, name: string = 'NamelessComponent'): Template {
    const parser = new NearleyParser(grammar);
    parser.feed(code);

    const nodes = parser.finishNodes();

    return new Template(
      name,
      nodes.filter(node => !(node instanceof ScriptTag) && !(node instanceof StyleTag)),
      nodes.find(node => node instanceof ScriptTag) as ScriptTag,
      nodes.find(node => node instanceof StyleTag) as StyleTag,
    );
  }
}
