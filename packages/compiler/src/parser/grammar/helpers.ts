import { Postprocessor } from 'nearley';
import stripIndent from 'strip-indent';
import ComponentElement from '../../ast/ComponentElement';
import EachStatement from '../../ast/EachStatement';
import { ExpressionAttribute, ListenerAttribute, LiteralAttribute } from '../../ast/element-attributes';
import HtmlElement from '../../ast/HtmlElement';
import IfStatement from '../../ast/IfStatement';
import PrintExpression from '../../ast/PrintExpression';
import ScriptTag from '../../ast/ScriptTag';
import Slot from '../../ast/Slot';
import StyleTag from '../../ast/StyleTag';
import Text from '../../ast/Text';

export const ignore: Postprocessor = () => null;

export const nodeList: Postprocessor = ([list]) => list.filter((item: any) => item !== null);

export const text: Postprocessor = ([token]) => {
  const value = token.value.replace(/\s+/g, ' ');

  if (value.trim().length === 0) {
    return null;
  }

  return new Text(value);
};

export const elementSelfclosing: Postprocessor = ([_, name, attributes]) => {
  const firstChar: string = name.value.charAt(0);

  if (firstChar.toUpperCase() === firstChar) {
    return new ComponentElement(name.value, attributes || []);
  }

  return new HtmlElement(name.value, attributes || []);
};

export const scriptTag: Postprocessor = ([_, content]) => new ScriptTag(content.value);

export const styleTag: Postprocessor = ([_, content]) => new StyleTag(content.value);

export const rawTag: Postprocessor = ([_, content]) => new Text(stripIndent(content.value).trim());

export const element: Postprocessor = ([start, children, end]) => {
  if (start.name !== end.name) {
    throw new Error(
      `The start tag "${start.name}" does not match the end tag "${end.name}"`,
    );
  }

  const firstChar: string = start.name.charAt(0);

  if (firstChar.toUpperCase() === firstChar) {
    return new ComponentElement(start.name, start.attributes, children);
  }

  return new HtmlElement(start.name, start.attributes, children);
};

export const elementStart: Postprocessor = ([_, name, attributes]) => ({
  name: name.value,
  attributes: attributes || [],
});

export const elementEnd: Postprocessor = ([_, __, name]) => ({
  name: name.value,
});

export const elementAttribute: Postprocessor = ([_, prefixToken, name, __, ___, value]) => {
  let prefix = '';

  if (prefixToken) {
    prefix = prefixToken[0].value;
  }

  switch (prefix) {
    case '@':
      return new ListenerAttribute(name.value, value.value);

    case ':':
      return new ExpressionAttribute(name.value, value.value);

    case '':
      return new LiteralAttribute(name.value, value.value);

    default:
      throw new Error(`Invalid prefix: "${prefix}"`);
  }
};

export const slot: Postprocessor = ([name, children, __]) => new Slot(name || 'default', children);

export const slotStart: Postprocessor = ([_, __, ___, name]) => name ? name[1].value : null;

export const printExpression: Postprocessor = ([_, expression, __]) =>
  new PrintExpression(expression.value.trim());

export const eachStatement: Postprocessor =
  ([{ initializer, expression, indexIdentifier }, nodes]) =>
    new EachStatement(initializer, expression, nodes, indexIdentifier);

export const eachStatementStart: Postprocessor =
  ([_, __, ___, ____, initializer, _____, expression]) => {
    const result = {
      expression: expression.value,
      initializer: '',
      indexIdentifier: undefined,
    };

    if (initializer.length === 1) {
      result.initializer = initializer[0].value;
    } else {
      result.initializer = initializer[2].value;
      result.indexIdentifier = initializer[6].value;
    }

    return result;
  };

export const ifStatement: Postprocessor = ([condition, thenBranch, elseBranch]) =>
  new IfStatement(condition, thenBranch, elseBranch ? elseBranch[1] : undefined);

export const ifStatementStart: Postprocessor = ([_, __, ___, condition]) => condition.value.trim();
