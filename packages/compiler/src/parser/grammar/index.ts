import { Grammar } from 'nearley';
import grammar from './grammar';

export default Grammar.fromCompiled(grammar);
