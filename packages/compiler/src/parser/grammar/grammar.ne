@{%
import lexer from '../lexer';
import * as helpers from './helpers';

const grammarLexer = lexer as any;
%}

@preprocessor typescript
@lexer grammarLexer

main -> node_list {% id %}

node_list -> node:+ {% helpers.nodeList %}
          | null

node -> %TEXT {% helpers.text %}
     | script_tag {% id %}
     | style_tag {% id %}
     | raw_tag {% id %}
     | element {% id %}
     | slot {% id %}
     | print_expression {% id %}
     | each_statement {% id %}
     | if_statement {% id %}

script_tag -> %SCRIPT_START %TEXT %SCRIPT_END {% helpers.scriptTag %}

style_tag -> %STYLE_START %TEXT %STYLE_END {% helpers.styleTag %}

raw_tag -> %RAW_START %TEXT %RAW_END {% helpers.rawTag %}

element -> element_start node_list element_end {% helpers.element %}
        | element_selfclosing {% id %}
element_selfclosing -> %TAG_START %IDENTIFIER element_attribute_list ws %SLASH %TAG_END {% helpers.elementSelfclosing %}
element_start -> %TAG_START %IDENTIFIER element_attribute_list ws %TAG_END {% helpers.elementStart %}
element_end -> %TAG_START %SLASH %IDENTIFIER %TAG_END {% helpers.elementEnd %}

element_attribute_list -> element_attribute:+ {% id %}
                       | null
element_attribute -> ws (%DOUBLE_COLON | %AT):? %IDENTIFIER %EQUALS %DOUBLE_QUOTE %TEXT %DOUBLE_QUOTE {% helpers.elementAttribute %}

slot -> slot_start node_list slot_end {% helpers.slot %}
slot_start -> %STATEMENT_START ws %SLOT (ws %TEXT):? ws %STATEMENT_END {% helpers.slotStart %}
slot_end -> %STATEMENT_START ws %ENDSLOT ws %STATEMENT_END {% helpers.ignore %}

print_expression -> %EXPRESSION_START %TEXT %EXPRESSION_END {% helpers.printExpression %}

each_statement -> each_statement_start node_list each_statement_end {% helpers.eachStatement %}
each_statement_start -> %STATEMENT_START ws %EACH ws (%LEFT_PAREN ws %TEXT ws %COMMA ws %TEXT ws %RIGHT_PAREN | %TEXT) %IN %TEXT ws %STATEMENT_END {% helpers.eachStatementStart %}
each_statement_end -> %STATEMENT_START ws %ENDEACH ws %STATEMENT_END {% helpers.ignore %}

if_statement -> if_statement_start node_list (if_statement_else node_list):? if_statement_end {% helpers.ifStatement %}
if_statement_start -> %STATEMENT_START ws %IF %TEXT %STATEMENT_END {% helpers.ifStatementStart %}
if_statement_else -> %STATEMENT_START ws %ELSE ws %STATEMENT_END {% helpers.ignore %}
if_statement_end -> %STATEMENT_START ws %ENDIF ws %STATEMENT_END {% helpers.ignore %}

ws -> null
   | %WS {% helpers.ignore %}