export class SyntaxError extends Error {
  constructor(message: string) {
    super(message);

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, SyntaxError.prototype);
  }
}
