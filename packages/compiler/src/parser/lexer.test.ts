'slot'; import lexer from './lexer';

describe('lexer', () => {
  it('should tokenize text', () => {
    lexer.reset('some text');

    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: 'some text' });
  });

  it('should switch states between tag tokens', () => {
    lexer.reset('before <> after');

    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: 'before ' });
    expect(lexer.next()).toMatchObject({ type: 'TAG_START', value: '<' });
    expect(lexer.next()).toMatchObject({ type: 'TAG_END', value: '>' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: ' after' });
  });

  it('should recognize tag tokens', () => {
    lexer.reset('<something something-ELSE-1337 /=   >');

    expect(lexer.next()).toMatchObject({ type: 'TAG_START', value: '<' });
    expect(lexer.next()).toMatchObject({
      type: 'IDENTIFIER',
      value: 'something',
    });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({
      type: 'IDENTIFIER',
      value: 'something-ELSE-1337',
    });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'SLASH', value: '/' });
    expect(lexer.next()).toMatchObject({ type: 'EQUALS', value: '=' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: '   ' });
    expect(lexer.next()).toMatchObject({ type: 'TAG_END', value: '>' });
  });

  it('should recognize script tags', () => {
    lexer.reset('<script>asdf < > </script>');

    expect(lexer.next()).toMatchObject({ type: 'SCRIPT_START', value: '<script>' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: 'asdf < > ' });
    expect(lexer.next()).toMatchObject({ type: 'SCRIPT_END', value: '</script>' });
  });

  it('should recognize style tags', () => {
    lexer.reset('<style>asdf < > </style>');

    expect(lexer.next()).toMatchObject({ type: 'STYLE_START', value: '<style>' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: 'asdf < > ' });
    expect(lexer.next()).toMatchObject({ type: 'STYLE_END', value: '</style>' });
  });

  it('should recognize raw tags', () => {
    lexer.reset('{% raw %}raw {% %} {% endraw %}');

    expect(lexer.next()).toMatchObject({ type: 'RAW_START', value: '{% raw %}' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: 'raw {% %} ' });
    expect(lexer.next()).toMatchObject({ type: 'RAW_END', value: '{% endraw %}' });
  });

  it('should recognize literals in tags', () => {
    lexer.reset('<"somethin !@#$%^&*()">');

    expect(lexer.next()).toMatchObject({ type: 'TAG_START', value: '<' });
    expect(lexer.next()).toMatchObject({ type: 'DOUBLE_QUOTE', value: '"' });
    expect(lexer.next()).toMatchObject({
      type: 'TEXT',
      value: 'somethin !@#$%^&*()',
    });
    expect(lexer.next()).toMatchObject({ type: 'DOUBLE_QUOTE', value: '"' });
    expect(lexer.next()).toMatchObject({ type: 'TAG_END', value: '>' });
  });

  it('should recognize double colons and at signs in tags', () => {
    lexer.reset('<:@>');

    expect(lexer.next()).toMatchObject({ type: 'TAG_START', value: '<' });
    expect(lexer.next()).toMatchObject({ type: 'DOUBLE_COLON', value: ':' });
    expect(lexer.next()).toMatchObject({ type: 'AT', value: '@' });
    expect(lexer.next()).toMatchObject({ type: 'TAG_END', value: '>' });
  });

  it('should recognize statements', () => {
    lexer.reset('{% slot henk %}{%endslot%}');

    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_START', value: '{%' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'SLOT', value: 'slot' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: 'henk' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_END', value: '%}' });

    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_START', value: '{%' });
    expect(lexer.next()).toMatchObject({ type: 'ENDSLOT', value: 'endslot' });
    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_END', value: '%}' });
  });

  it('should recognize each statements #1', () => {
    lexer.reset('{% each (item, index) in list %}{% endeach %}');

    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_START', value: '{%' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'EACH', value: 'each' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'LEFT_PAREN', value: '(' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: 'item' });
    expect(lexer.next()).toMatchObject({ type: 'COMMA', value: ',' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: 'index' });
    expect(lexer.next()).toMatchObject({ type: 'RIGHT_PAREN', value: ')' });
    expect(lexer.next()).toMatchObject({ type: 'IN', value: ' in ' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: 'list' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_END', value: '%}' });
  });

  it('should recognize each statements #2', () => {
    lexer.reset('{% each item in list %}{% endeach %}');

    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_START', value: '{%' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'EACH', value: 'each' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: 'item' });
    expect(lexer.next()).toMatchObject({ type: 'IN', value: ' in ' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: 'list' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_END', value: '%}' });
  });

  it('should recognize each statements #2', () => {
    lexer.reset('{% each item in list %}{% endeach %}');

    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_START', value: '{%' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'EACH', value: 'each' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: 'item' });
    expect(lexer.next()).toMatchObject({ type: 'IN', value: ' in ' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: 'list' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_END', value: '%}' });
  });

  it('should recognize each statements #3', () => {
    lexer.reset('{% each item in list.something(first, second) %}{% endeach %}');

    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_START', value: '{%' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'EACH', value: 'each' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: 'item' });
    expect(lexer.next()).toMatchObject({ type: 'IN', value: ' in ' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: 'list.something(first,' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: 'second)' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_END', value: '%}' });
  });

  it('should recognize expressions', () => {
    lexer.reset('{{something}}{{ something else }}');

    expect(lexer.next()).toMatchObject({ type: 'EXPRESSION_START', value: '{{' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: 'something' });
    expect(lexer.next()).toMatchObject({ type: 'EXPRESSION_END', value: '}}' });
    expect(lexer.next()).toMatchObject({ type: 'EXPRESSION_START', value: '{{' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: ' something else ' });
    expect(lexer.next()).toMatchObject({ type: 'EXPRESSION_END', value: '}}' });
  });

  it('should recognize if statements', () => {
    lexer.reset('{% if something %}{% endif %}');

    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_START', value: '{%' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'IF', value: 'if' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: ' something ' });
    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_END', value: '%}' });
    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_START', value: '{%' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'ENDIF', value: 'endif' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_END', value: '%}' });
  });

  it('should recognize if statements with else branches', () => {
    lexer.reset('{% if something %}{% else %}{% endif %}');

    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_START', value: '{%' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'IF', value: 'if' });
    expect(lexer.next()).toMatchObject({ type: 'TEXT', value: ' something ' });
    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_END', value: '%}' });
    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_START', value: '{%' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'ELSE', value: 'else' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_END', value: '%}' });
    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_START', value: '{%' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'ENDIF', value: 'endif' });
    expect(lexer.next()).toMatchObject({ type: 'WS', value: ' ' });
    expect(lexer.next()).toMatchObject({ type: 'STATEMENT_END', value: '%}' });
  });
});
