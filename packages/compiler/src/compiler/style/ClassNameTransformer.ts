import { createStringLiteral, Expression, isStringLiteral, Node, transform, visitEachChild } from 'typescript';
import ComponentElement from '../../ast/ComponentElement';
import EachStatement from '../../ast/EachStatement';
import { ElementAttribute, ExpressionAttribute, LiteralAttribute } from '../../ast/element-attributes';
import HtmlElement from '../../ast/HtmlElement';
import IfStatement from '../../ast/IfStatement';
import TemplateNode from '../../ast/TemplateNode';
import { parseExpression } from '../script/utils/parser';
import { printNode } from '../script/utils/printer';

export type ReplacementMap = Record<string, string>;

export default class ClassNameTransformer {
  public transform(nodes: TemplateNode[], replacements: ReplacementMap): TemplateNode[] {
    return nodes.map(node => this.transformNode(node, replacements));
  }

  private transformNode(node: TemplateNode, replacements: ReplacementMap): TemplateNode {
    if (node instanceof HtmlElement) {
      return new HtmlElement(
        node.name,
        this.transformAttributes(node.attributes, replacements),
        this.transform(node.children, replacements),
      );
    }

    if (node instanceof ComponentElement) {
      return new ComponentElement(
        node.name,
        this.transformAttributes(node.attributes, replacements),
        this.transform(node.children, replacements),
      );
    }

    if (node instanceof EachStatement) {
      return new EachStatement(
        node.initializer,
        node.expression,
        this.transform(node.children, replacements),
        node.indexIdentifier,
      );
    }

    if (node instanceof IfStatement) {
      return new IfStatement(
        node.condition,
        this.transform(node.thenBranch, replacements),
        node.elseBranch && this.transform(node.elseBranch, replacements),
      );
    }

    return node;
  }

  private transformAttributes(
    attributes: ElementAttribute[],
    replacements: ReplacementMap,
  ): ElementAttribute[] {
    return attributes.map(attribute => this.transformAttribute(attribute, replacements));
  }

  private transformAttribute(
    attribute: ElementAttribute,
    replacements: ReplacementMap,
  ): ElementAttribute {
    if (attribute.name !== 'class') {
      return attribute;
    }

    if (attribute instanceof LiteralAttribute) {
      return this.transformLiteralAttribute(attribute, replacements);
    }

    if (attribute instanceof ExpressionAttribute) {
      return this.transformExpressionAttribute(attribute, replacements);
    }

    return attribute;
  }

  private transformLiteralAttribute(
    attribute: LiteralAttribute,
    replacements: ReplacementMap,
  ): LiteralAttribute {
    return new LiteralAttribute(
      attribute.name,
      this.replaceClassNames(attribute.value, replacements),
    );
  }

  private transformExpressionAttribute(
    attribute: ExpressionAttribute,
    replacements: ReplacementMap,
  ): ExpressionAttribute {
    const expression = parseExpression(attribute.value);

    const transformed = transform(expression, [
      (context) => (n) => {
        const visit = (node: Node): Node => {
          if (isStringLiteral(node)) {
            return createStringLiteral(this.replaceClassNames(node.text, replacements));
          }

          return visitEachChild(node, visit, context);
        };

        return visit(n) as Expression;
      },
    ]);

    return new ExpressionAttribute(
      attribute.name,
      printNode(transformed.transformed[0]),
    );
  }

  private replaceClassNames(original: string, replacements: ReplacementMap): string {
    return original
      .split(' ')
      .map(part => replacements[part] || part)
      .join(' ');
  }
}
