import Template from '../../ast/Template';
import { CompilerOptions } from '../CompilerOptions';
import ClassNameTransformer from './ClassNameTransformer';
import { NameMangler } from './NameMangler';

export default class StyleBuilder {
  private classNameTransformer = new ClassNameTransformer();

  public build(template: Template, options: CompilerOptions): string | undefined {
    if (!template.style) {
      return;
    }

    const nameMangler = new NameMangler(options.shortPropertyNames);
    const [scss, replacements] = nameMangler.mangle(template.style.value);

    template.nodes = this.classNameTransformer.transform(template.nodes, replacements);

    return scss;
  }
}
