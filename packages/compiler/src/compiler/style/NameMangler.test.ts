import { NameMangler } from './NameMangler';

describe('NameMangler', () => {
  it('should mangle classnames', () => {
    const raw = `
      .some-name {
        .nested-name.appended .child {}
      }
    `;

    const mangler = new NameMangler();
    const [scss, replacements] = mangler.mangle(raw);

    expect(scss).toEqual(`
      ._some-name_1sorruh_2 {
        ._nested-name_1sorruh_3._appended_1sorruh_3 ._child_1sorruh_3 {}
      }
    `);

    expect(replacements).toEqual({
      'some-name': '_some-name_1sorruh_2',
      'nested-name': '_nested-name_1sorruh_3',
      appended: '_appended_1sorruh_3',
      child: '_child_1sorruh_3',
    });
  });

  it('should be able to mangle classnames with shorter names', () => {
    const raw = `
      .some-name {
        .nested-name.appended .child {}
      }
    `;

    const mangler = new NameMangler(true);
    const [scss, replacements] = mangler.mangle(raw);

    expect(scss).toEqual(`
      ._1y51ghn1sorruh2 {
        ._noue9u1sorruh3._ceu2ju1sorruh3 ._2puvrj1sorruh3 {}
      }
    `);

    expect(replacements).toEqual({
      'some-name': '_1y51ghn1sorruh2',
      'nested-name': '_noue9u1sorruh3',
      appended: '_ceu2ju1sorruh3',
      child: '_2puvrj1sorruh3',
    });
  });
});
