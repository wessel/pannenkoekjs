import { Node, parse, stringify } from 'scss-parser';
import stringHash from 'string-hash';

type ReplacementMap = Record<string, string>;

export class NameMangler {
  constructor(private shortPropertyNames?: boolean) { }

  public mangle(scss: string): [string, ReplacementMap] {
    const replacements: ReplacementMap = {};
    const transformed = this.transformNode(
      parse(scss),
      stringHash(scss).toString(36),
      replacements,
    );

    return [stringify(transformed), replacements];
  }

  private transformNode(node: Node, scssHash: string, replacements: ReplacementMap): Node {
    if (node.type === 'class' && Array.isArray(node.value)) {
      return {
        type: 'class',
        value: node.value.map(child => {
          if (child.type === 'identifier' && typeof (child.value) === 'string') {
            if (!replacements[child.value]) {
              replacements[child.value] = this.generateName(
                child.value,
                scssHash,
                node.start!.line,
              );
            }

            return {
              type: child.type,
              value: replacements[child.value],
            };
          }

          return this.transformNode(child, scssHash, replacements);
        }),
      };
    }

    if (Array.isArray(node.value)) {
      return {
        type: node.type,
        value: node.value.map(child => this.transformNode(child, scssHash, replacements)),
      };
    }

    return node;
  }

  private generateName(name: string, scssHash: string, lineNumber: number): string {
    if (this.shortPropertyNames) {
      return `_${stringHash(name).toString(36)}${scssHash}${lineNumber}`;
    }

    return `_${name}_${scssHash}_${lineNumber}`;
  }
}
