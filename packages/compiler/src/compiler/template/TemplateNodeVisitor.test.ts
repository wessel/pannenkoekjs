import Text from '../../ast/Text';
import Block from '../code-builder/Block';
import SourceFile from '../code-builder/SourceFile';
import { DefaultNameGenerator } from './NameGenerator';
import TemplateNodeVisitor from './TemplateNodeVisitor';

describe('TemplateNodeVisitor', () => {
  it('asdf', () => {
    const visitor = new TemplateNodeVisitor(new DefaultNameGenerator());
    visitor.visit(new Text('asdf'), {
      currentBlock: new Block('TestBlock'),
      sourceFile: new SourceFile(),
      metadata: {
        globals: [],
        properties: [],
      },
    });
  });
});
