import ComponentElement from '../../ast/ComponentElement';
import EachStatement from '../../ast/EachStatement';
import HtmlElement from '../../ast/HtmlElement';
import IfStatement from '../../ast/IfStatement';
import PrintExpression from '../../ast/PrintExpression';
import Slot from '../../ast/Slot';
import TemplateNode from '../../ast/TemplateNode';
import Text from '../../ast/Text';
import ComponentElementHandler from './handler/ComponentElementHandler';
import EachStatementHandler from './handler/EachStatementHandler';
import Handler from './handler/Handler';
import HtmlElementHandler from './handler/html-element/HtmlElementHandler';
import IfStatementHandler from './handler/IfStatementHandler';
import PrintExpressionHandler from './handler/PrintExpressionHandler';
import SlotHandler from './handler/SlotHandler';
import TextHandler from './handler/TextHandler';
import { NameGenerator } from './NameGenerator';
import VisitContext from './VisitContext';

export default class TemplateNodeVisitor {
  private map: Map<any, Handler<TemplateNode>>;

  constructor(nameGenerator: NameGenerator) {
    this.map = new Map();
    this.map.set(Text, new TextHandler(nameGenerator));
    this.map.set(HtmlElement, new HtmlElementHandler(nameGenerator));
    this.map.set(ComponentElement, new ComponentElementHandler(nameGenerator));
    this.map.set(Slot, new SlotHandler(nameGenerator));
    this.map.set(PrintExpression, new PrintExpressionHandler(nameGenerator));
    this.map.set(EachStatement, new EachStatementHandler(nameGenerator));
    this.map.set(IfStatement, new IfStatementHandler(nameGenerator));
  }

  public visit(node: TemplateNode, context: VisitContext) {
    this.getHandler(node).build(node, this, context);
  }

  private getHandler<T extends TemplateNode>(node: T): Handler<T> {
    const nodeConstructor: new () => T = Object.getPrototypeOf(node).constructor;
    const handler = this.map.get(nodeConstructor);

    if (!handler) {
      throw new Error(`Could not find a handler for "${nodeConstructor.name}"`);
    }

    return handler;
  }
}
