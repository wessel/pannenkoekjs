import Template from '../../ast/Template';
import Block from '../code-builder/Block';
import SourceFile from '../code-builder/SourceFile';
import { CompilerOptions } from '../CompilerOptions';
import { ComponentScriptMetaData } from '../script/analyzer/ComponentScriptAnalyzer';
import { DefaultNameGenerator, ShortNameGenerator } from './NameGenerator';
import TemplateNodeVisitor from './TemplateNodeVisitor';
import VisitContext from './VisitContext';

export default class TemplateBuilder {
  public build(
    template: Template,
    metadata: ComponentScriptMetaData,
    output: SourceFile,
    options: CompilerOptions,
  ) {
    const nameGenerator = options.shortPropertyNames
      ? new ShortNameGenerator()
      : new DefaultNameGenerator();
    const visitor = new TemplateNodeVisitor(nameGenerator);

    const mainBlock = new Block('MainBlock');
    output.addClass(mainBlock);

    const context: VisitContext = {
      metadata,
      currentBlock: mainBlock,
      sourceFile: output,
    };

    template.nodes.forEach(node => visitor.visit(node, context));
  }
}
