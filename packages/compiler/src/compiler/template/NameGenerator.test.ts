import Text from '../../ast/Text';
import Block from '../code-builder/Block';
import { DefaultNameGenerator, NodeNameGenerator } from './NameGenerator';

describe('NodeNameGenerator', () => {
  test('generate', () => {
    const generator = new NodeNameGenerator();

    const text0 = new Text('');
    const text1 = new Text('');
    const text2 = new Text('');

    expect(generator.generate(text0)).toEqual('text_0');
    expect(generator.generate(text0)).toEqual('text_0');
    expect(generator.generate(text1)).toEqual('text_1');
    expect(generator.generate(text1)).toEqual('text_1');
    expect(generator.generate(text2)).toEqual('text_2');
    expect(generator.generate(text2)).toEqual('text_2');
  });
});

describe('DefaultNameGenerator', () => {
  test('generateScoped', () => {
    const generator = new DefaultNameGenerator();

    const scope1 = new Block('Scope1');
    const scope2 = new Block('Scope2');

    const text0 = new Text('');
    const text1 = new Text('');

    expect(generator.generateScoped(text0, scope1)).toEqual('text_0');
    expect(generator.generateScoped(text0, scope1)).toEqual('text_0');
    expect(generator.generateScoped(text0, scope2)).toEqual('text_0');
    expect(generator.generateScoped(text0, scope2)).toEqual('text_0');

    expect(generator.generateScoped(text1, scope1)).toEqual('text_1');
    expect(generator.generateScoped(text1, scope2)).toEqual('text_1');
  });

  test('generateScoped with suffix', () => {
    const generator = new DefaultNameGenerator();

    const scope1 = new Block('Scope1');
    const scope2 = new Block('Scope2');

    const text0 = new Text('');
    const text1 = new Text('');

    expect(generator.generateScoped(text0, scope1, 'test')).toEqual('text_0_test');
    expect(generator.generateScoped(text0, scope2, 'test')).toEqual('text_0_test');

    expect(generator.generateScoped(text1, scope1, 'test')).toEqual('text_1_test');
    expect(generator.generateScoped(text1, scope2, 'test')).toEqual('text_1_test');
  });

  test('generate', () => {
    const generator = new DefaultNameGenerator();

    const text0 = new Text('');
    const text1 = new Text('');

    expect(generator.generate(text0)).toEqual('Text_0');
    expect(generator.generate(text1)).toEqual('Text_1');
  });

  test('generate with suffix', () => {
    const generator = new DefaultNameGenerator();

    const text0 = new Text('');
    const text1 = new Text('');

    expect(generator.generate(text0, 'After')).toEqual('Text_0_After');
    expect(generator.generate(text1, 'After')).toEqual('Text_1_After');
  });
});
