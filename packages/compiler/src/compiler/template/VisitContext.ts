import TemplateNode from '../../ast/TemplateNode';
import Block from '../code-builder/Block';
import SourceFile from '../code-builder/SourceFile';
import { ComponentScriptMetaData } from '../script/analyzer/ComponentScriptAnalyzer';

export default interface VisitContext {
  parent?: TemplateNode;
  currentBlock: Block;
  sourceFile: SourceFile;
  metadata: ComponentScriptMetaData;
}
