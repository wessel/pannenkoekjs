import EachStatement from '../../../ast/EachStatement';
import Block from '../../code-builder/Block';
import ClassProperty from '../../code-builder/property/ClassProperty';
import SourceFile from '../../code-builder/SourceFile';
import { DefaultNameGenerator } from '../NameGenerator';
import EachStatementHandler from './EachStatementHandler';
import { createHandlerTest, HandlerTest } from './test-utils';

describe('Each statement handler', () => {
  let handlerTest: HandlerTest<EachStatement>;

  beforeEach(() => {
    handlerTest = createHandlerTest(
      EachStatement,
      new EachStatementHandler(new DefaultNameGenerator()),
    );
  });

  test('each statement', () => {
    const node = new EachStatement('item', 'list', []);

    const expectedBlock = new Block('TestBlock');
    expectedBlock.addProperty(new ClassProperty(
      'eachstatement_0',
      'EachStatementWrapper',
      undefined,
      true,
    ));
    expectedBlock.getMount().add(`
      this.eachstatement_0 = new EachStatementWrapper(
        "item",
        () => this.ctx,
        Eachstatement_0,
        () => this.ctx.list,
      );
    `);
    expectedBlock.getMount().add('target.appendChild(this.eachstatement_0.anchor);');
    expectedBlock.getMount().add('this.eachstatement_0.evaluate();');
    expectedBlock.getUpdate().add('this.eachstatement_0.evaluate();');
    expectedBlock.getRemove().add('this.eachstatement_0.remove();');

    expect(handlerTest(node)).toEqual(new SourceFile(
      [expectedBlock, new Block('Eachstatement_0')],
      [{ name: 'EachStatementWrapper', from: '@pannenkoekjs/runtime' }],
    ));
  });
});
