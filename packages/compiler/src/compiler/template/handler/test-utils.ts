import TemplateNode from '../../../ast/TemplateNode';
import Block from '../../code-builder/Block';
import SourceFile from '../../code-builder/SourceFile';
import { ComponentScriptMetaData } from '../../script/analyzer/ComponentScriptAnalyzer';
import TemplateNodeVisitor from '../TemplateNodeVisitor';
import VisitContext from '../VisitContext';
import Handler from './Handler';

export class HandlerTestNode implements TemplateNode {
  constructor(public readonly id: string) { }
}

export type HandlerTest<T extends TemplateNode> = (
  node: T,
  parent?: TemplateNode,
  metadata?: ComponentScriptMetaData,
) => SourceFile;

export const createHandlerTest =
  <T>(nodeConstructor: new (...args: any[]) => T, handler: Handler<T>): HandlerTest<T> =>
    (node: T, parent?: TemplateNode, metadata?: ComponentScriptMetaData): SourceFile => {
      const visitor = new MockTemplateNodeVisitor(nodeConstructor, handler);

      const file = new SourceFile();
      const currentBlock = new Block('TestBlock');
      file.addClass(currentBlock);

      const context: VisitContext = {
        parent,
        currentBlock,
        sourceFile: file,
        metadata: metadata || {
          globals: [],
          properties: [],
        },
      };

      visitor.visit(node, context);

      return file;
    };

class MockTemplateNodeVisitor<T> {
  constructor(private nodeConstructor: new (...args: any[]) => T, private handler: Handler<T>) { }

  public visit(node: TemplateNode, context: VisitContext): void {
    if (node instanceof this.nodeConstructor) {
      this.handler.build(node, this as any as TemplateNodeVisitor, context);
    } else if (node instanceof HandlerTestNode) {
      // todo: do nothing?
    } else {
      throw new Error('Method not implemented.');
    }
  }
}
