import { createIdentifier } from 'typescript';
import ComponentElement from '../../../ast/ComponentElement';
import { ExpressionAttribute, LiteralAttribute } from '../../../ast/element-attributes';
import Block from '../../code-builder/Block';
import ClassProperty from '../../code-builder/property/ClassProperty';
import { ComponentScriptMetaData } from '../../script/analyzer/ComponentScriptAnalyzer';
import { prefixExpression } from '../../script/transform/expression-prefixer';
import { NameGenerator } from '../NameGenerator';
import TemplateNodeVisitor from '../TemplateNodeVisitor';
import VisitContext from '../VisitContext';
import Handler from './Handler';

export default class ComponentElementHandler implements Handler<ComponentElement> {
  constructor(private nameGenerator: NameGenerator) { }

  public build(node: ComponentElement, visitor: TemplateNodeVisitor, context: VisitContext) {
    context.sourceFile.addRuntimeImport('ComponentElementWrapper');

    const propertyName = this.nameGenerator.generateScoped(node, context.currentBlock);
    const constructorCallback = context.metadata.properties.includes(node.name)
      ? `ctx.${node.name}`
      : node.name;

    const defaultSlot = this.createDefaultSlot(node, visitor, context);

    context.currentBlock.addProperty(new ClassProperty(
      propertyName,
      'ComponentElementWrapper',
      undefined,
      true,
    ));

    context.currentBlock.getMount().add(`
      this.${propertyName} = new ComponentElementWrapper(
        () => this.ctx,
        this.slots,
        () => ${constructorCallback},
        ${defaultSlot ? defaultSlot.name : 'undefined'},
      );
    `);

    if (context.parent) {
      const parentName = this.nameGenerator.generateScoped(context.parent, context.currentBlock);

      context.currentBlock.getMount().add(`this.${parentName}.appendChild(this.${propertyName}.anchor);`);
    } else {
      context.currentBlock.getMount().add(`target.appendChild(this.${propertyName}.anchor);`);
    }

    context.currentBlock.getMount().add(`this.${propertyName}.evaluate(${this.buildPropsInitializer(node, context.metadata)});`);
    context.currentBlock.getUpdate().add(`this.${propertyName}.update(ctx, ${this.buildPropsInitializer(node, context.metadata)});`);
    context.currentBlock.getRemove().add(`this.${propertyName}.remove();`);

    this.buildAttributes(node, context.currentBlock, propertyName, context.metadata);
  }

  private createDefaultSlot(
    node: ComponentElement,
    visitor: TemplateNodeVisitor,
    context: VisitContext,
  ): Block | undefined {
    if (node.children.length === 0) {
      return;
    }

    const name = this.nameGenerator.generate(node);
    const block = new Block(name);
    const childContext: VisitContext = { ...context, parent: undefined, currentBlock: block };

    context.sourceFile.addClass(block);

    node.children.forEach(child => visitor.visit(child, childContext));

    return block;
  }

  private buildAttributes(
    node: ComponentElement,
    block: Block,
    propertyName: string,
    metadata: ComponentScriptMetaData,
  ) {
    node.attributes.forEach(attribute => {
      if (attribute instanceof ExpressionAttribute) {
        const expression = prefixExpression(
          attribute.value,
          createIdentifier('this.ctx'),
          metadata.globals,
        );

        block.getUpdate().add(`(this.${propertyName}.instance! as any).${attribute.name} = ${expression};`);
      }
    });
  }

  private buildPropsInitializer(node: ComponentElement, metadata: ComponentScriptMetaData) {
    const propsInitializer: string[] = [];

    node.attributes.forEach(attribute => {
      if (attribute instanceof LiteralAttribute) {
        propsInitializer.push(`${attribute.name}: ${JSON.stringify(attribute.value)}`);
      } else if (attribute instanceof ExpressionAttribute) {
        const expression = prefixExpression(
          attribute.value,
          createIdentifier('ctx'),
          metadata.globals,
        );

        propsInitializer.push(`${attribute.name}: ${expression}`);
      }
    });

    return `{ ${propsInitializer.join(', ')} }`;
  }
}
