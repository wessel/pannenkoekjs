import HtmlElement from '../../../ast/HtmlElement';
import Text from '../../../ast/Text';
import Block from '../../code-builder/Block';
import TextNodeProperty from '../../code-builder/property/TextNodeProperty';
import SourceFile from '../../code-builder/SourceFile';
import { DefaultNameGenerator } from '../NameGenerator';
import { createHandlerTest, HandlerTest } from './test-utils';
import TextHandler from './TextHandler';

describe('TextHandler', () => {
  let handlerTest: HandlerTest<Text>;

  beforeEach(() => {
    handlerTest = createHandlerTest(Text, new TextHandler(new DefaultNameGenerator()));
  });

  test('simple text', () => {
    const node = new Text('something');

    const expectedBlock = new Block('TestBlock');
    expectedBlock.addElement(new TextNodeProperty('text_0', 'something'));

    expect(handlerTest(node)).toEqual(new SourceFile([expectedBlock]));
  });

  test('simple text with parent', () => {
    const node = new Text('something');
    const parent = new Text('something else');

    const expectedBlock = new Block('TestBlock');
    expectedBlock.addElement(
      new TextNodeProperty('text_0', 'something'),
      'text_1',
    );

    expect(handlerTest(node, parent)).toEqual(new SourceFile([expectedBlock]));
  });

  test('parent with single child', () => {
    const node = new Text('something');
    const parent = new HtmlElement('div', [], [node]);

    const expectedBlock = new Block('TestBlock');
    expectedBlock.getMount().add('this.div_0.textContent = "something";');

    expect(handlerTest(node, parent)).toEqual(new SourceFile([expectedBlock]));
  });
});
