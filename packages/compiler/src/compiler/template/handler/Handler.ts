import TemplateNode from '../../../ast/TemplateNode';
import TemplateNodeVisitor from '../TemplateNodeVisitor';
import VisitContext from '../VisitContext';

export default interface Handler<N extends TemplateNode> {
  build(node: N, visitor: TemplateNodeVisitor, context: VisitContext): void;
}
