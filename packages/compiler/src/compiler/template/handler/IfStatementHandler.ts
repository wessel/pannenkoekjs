import { createIdentifier } from 'typescript';
import IfStatement from '../../../ast/IfStatement';
import TemplateNode from '../../../ast/TemplateNode';
import Block from '../../code-builder/Block';
import ClassProperty from '../../code-builder/property/ClassProperty';
import { prefixExpression } from '../../script/transform/expression-prefixer';
import { NameGenerator } from '../NameGenerator';
import TemplateNodeVisitor from '../TemplateNodeVisitor';
import VisitContext from '../VisitContext';
import Handler from './Handler';

export default class IfStatementHandler implements Handler<IfStatement> {
  constructor(private nameGenerator: NameGenerator) { }

  public build(node: IfStatement, visitor: TemplateNodeVisitor, context: VisitContext): void {
    context.sourceFile.addRuntimeImport('IfStatementWrapper');

    const propertyName = this.nameGenerator.generateScoped(node, context.currentBlock);

    context.currentBlock.addProperty(new ClassProperty(
      propertyName,
      'IfStatementWrapper',
      undefined,
      true,
    ));

    const blockName = this.nameGenerator.generate(node);
    const thenBranch = this.createBlock(`${blockName}Then`, node.thenBranch, context, visitor);
    const elseBranch = node.elseBranch
      && this.createBlock(`${blockName}Else`, node.elseBranch, context, visitor);

    const prefixedExpression = prefixExpression(node.condition, createIdentifier('this.ctx'));

    context.currentBlock.getMount().add(`
      this.${propertyName} = new IfStatementWrapper(
        this.slots,
        () => this.ctx,
        () => ${prefixedExpression},
        ${thenBranch.name},
        ${elseBranch ? elseBranch.name : 'undefined'},
      );
    `);

    if (context.parent) {
      const parentName = this.nameGenerator.generateScoped(context.parent, context.currentBlock);

      context.currentBlock.getMount().add(`this.${parentName}.appendChild(this.${propertyName}.anchor);`);
    } else {
      context.currentBlock.getMount().add(`target.appendChild(this.${propertyName}.anchor);`);
    }

    context.currentBlock.getMount().add(`this.${propertyName}.evaluate();`);
    context.currentBlock.getUpdate().add(`this.${propertyName}.evaluate();`);
    context.currentBlock.getRemove().add(`this.${propertyName}.remove();`);
  }

  private createBlock(
    name: string,
    nodes: TemplateNode[],
    context: VisitContext,
    visitor: TemplateNodeVisitor,
  ): Block {
    const block = new Block(name);
    context.sourceFile.addClass(block);

    const childCtx: VisitContext = { ...context, parent: undefined, currentBlock: block };
    nodes.forEach(node => visitor.visit(node, childCtx));

    return block;
  }
}
