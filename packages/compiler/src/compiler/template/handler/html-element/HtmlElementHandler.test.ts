import { ExpressionAttribute, ListenerAttribute, LiteralAttribute } from '../../../../ast/element-attributes';
import HtmlElement from '../../../../ast/HtmlElement';
import Block from '../../../code-builder/Block';
import ElementNodeProperty from '../../../code-builder/property/ElementNodeProperty';
import SourceFile from '../../../code-builder/SourceFile';
import { DefaultNameGenerator } from '../../NameGenerator';
import { createHandlerTest, HandlerTest } from '../test-utils';
import HtmlElementHandler from './HtmlElementHandler';

describe('HtmlElementHandler', () => {
  let handlerTest: HandlerTest<HtmlElement>;

  beforeEach(() => {
    handlerTest = createHandlerTest(
      HtmlElement,
      new HtmlElementHandler(new DefaultNameGenerator()),
    );
  });

  test('simple html element', () => {
    const node = new HtmlElement('div');

    const expectedBlock = new Block('TestBlock');
    expectedBlock.addElement(new ElementNodeProperty('div_0', 'div'));

    expect(handlerTest(node)).toEqual(new SourceFile([expectedBlock]));
  });

  test('html element with attributes', () => {
    const node = new HtmlElement('div', [
      new LiteralAttribute('something', 'nothing'),
      new ExpressionAttribute('expression', 'test'),
      new ListenerAttribute('listener', 'something()'),
    ]);

    const expectedBlock = new Block('TestBlock');
    expectedBlock.getMount().add('this.div_0.setAttribute("something", "nothing");');
    expectedBlock.getMount().add('this.div_0.setAttribute("expression", ctx.test);');
    expectedBlock.getMount().add('this.div_0.addEventListener("listener", ($$event: any) => { this.ctx.something() });');
    expectedBlock.addElement(new ElementNodeProperty('div_0', 'div'));
    expectedBlock.getUpdate().add('this.div_0.setAttribute("expression", ctx.test);');

    expect(handlerTest(node)).toEqual(new SourceFile([expectedBlock]));
  });

  test('html element with ref attribute', () => {
    const node = new HtmlElement('div', [
      new LiteralAttribute('ref', 'something'),
    ]);

    const expectedBlock = new Block('TestBlock');
    expectedBlock.getMount().add('this.ctx.$$component.refs["something"] = this.div_0;');
    expectedBlock.getRemove().add('delete this.ctx.$$component.refs["something"];');
    expectedBlock.addElement(new ElementNodeProperty('div_0', 'div'));

    expect(handlerTest(node)).toEqual(new SourceFile([expectedBlock]));
  });
});
