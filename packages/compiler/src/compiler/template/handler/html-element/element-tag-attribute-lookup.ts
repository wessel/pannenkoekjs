interface AttributeLookupObject {
  elements?: string[];
  name?: string;
}

/**
 * This attribute lookup map is a modified version of the `attribute_lookup`
 * in Svelte's `AttributeWrapper`
 */
export const TAG_ATTRIBUTE_LOOKUP: Record<string, AttributeLookupObject> = {
  accept: { elements: ['form', 'input'] },
  'accept-charset': { name: 'acceptCharset', elements: ['form'] },
  accesskey: { name: 'accessKey' },
  action: { elements: ['form'] },
  align: {
    elements: [
      'applet',
      'caption',
      'col',
      'colgroup',
      'hr',
      'iframe',
      'img',
      'table',
      'tbody',
      'td',
      'tfoot',
      'th',
      'thead',
      'tr',
    ],
  },
  allowfullscreen: { name: 'allowFullscreen', elements: ['iframe'] },
  alt: { elements: ['applet', 'area', 'img', 'input'] },
  async: { elements: ['script'] },
  autocomplete: { elements: ['form', 'input'] },
  autofocus: { elements: ['button', 'input', 'keygen', 'select', 'textarea'] },
  autoplay: { elements: ['audio', 'video'] },
  autosave: { elements: ['input'] },
  bgcolor: {
    name: 'bgColor',
    elements: [
      'body',
      'col',
      'colgroup',
      'marquee',
      'table',
      'tbody',
      'tfoot',
      'td',
      'th',
      'tr',
    ],
  },
  border: { elements: ['img', 'object', 'table'] },
  buffered: { elements: ['audio', 'video'] },
  challenge: { elements: ['keygen'] },
  charset: { elements: ['meta', 'script'] },
  checked: { elements: ['command', 'input'] },
  cite: { elements: ['blockquote', 'del', 'ins', 'q'] },
  class: { name: 'className' },
  code: { elements: ['applet'] },
  codebase: { name: 'codeBase', elements: ['applet'] },
  color: { elements: ['basefont', 'font', 'hr'] },
  cols: { elements: ['textarea'] },
  colspan: { name: 'colSpan', elements: ['td', 'th'] },
  content: { elements: ['meta'] },
  contenteditable: { name: 'contentEditable' },
  contextmenu: {},
  controls: { elements: ['audio', 'video'] },
  coords: { elements: ['area'] },
  data: { elements: ['object'] },
  datetime: { name: 'dateTime', elements: ['del', 'ins', 'time'] },
  default: { elements: ['track'] },
  defer: { elements: ['script'] },
  dir: {},
  dirname: { name: 'dirName', elements: ['input', 'textarea'] },
  disabled: {
    elements: [
      'button',
      'command',
      'fieldset',
      'input',
      'keygen',
      'optgroup',
      'option',
      'select',
      'textarea',
    ],
  },
  download: { elements: ['a', 'area'] },
  draggable: {},
  dropzone: {},
  enctype: { elements: ['form'] },
  for: { name: 'htmlFor', elements: ['label', 'output'] },
  formaction: { elements: ['input', 'button'] },
  headers: { elements: ['td', 'th'] },
  height: {
    elements: ['canvas', 'embed', 'iframe', 'img', 'input', 'object', 'video'],
  },
  hidden: {},
  high: { elements: ['meter'] },
  href: { elements: ['a', 'area', 'base', 'link'] },
  hreflang: { elements: ['a', 'area', 'link'] },
  'http-equiv': { name: 'httpEquiv', elements: ['meta'] },
  icon: { elements: ['command'] },
  id: {},
  indeterminate: { elements: ['input'] },
  ismap: { name: 'isMap', elements: ['img'] },
  itemprop: {},
  keytype: { elements: ['keygen'] },
  kind: { elements: ['track'] },
  label: { elements: ['track'] },
  lang: {},
  language: { elements: ['script'] },
  loop: { elements: ['audio', 'bgsound', 'marquee', 'video'] },
  low: { elements: ['meter'] },
  manifest: { elements: ['html'] },
  max: { elements: ['input', 'meter', 'progress'] },
  maxlength: { name: 'maxLength', elements: ['input', 'textarea'] },
  media: { elements: ['a', 'area', 'link', 'source', 'style'] },
  method: { elements: ['form'] },
  min: { elements: ['input', 'meter'] },
  multiple: { elements: ['input', 'select'] },
  muted: { elements: ['audio', 'video'] },
  name: {
    elements: [
      'button',
      'form',
      'fieldset',
      'iframe',
      'input',
      'keygen',
      'object',
      'output',
      'select',
      'textarea',
      'map',
      'meta',
      'param',
    ],
  },
  novalidate: { name: 'noValidate', elements: ['form'] },
  open: { elements: ['details'] },
  optimum: { elements: ['meter'] },
  pattern: { elements: ['input'] },
  ping: { elements: ['a', 'area'] },
  placeholder: { elements: ['input', 'textarea'] },
  poster: { elements: ['video'] },
  preload: { elements: ['audio', 'video'] },
  radiogroup: { elements: ['command'] },
  readonly: { name: 'readOnly', elements: ['input', 'textarea'] },
  rel: { elements: ['a', 'area', 'link'] },
  required: { elements: ['input', 'select', 'textarea'] },
  reversed: { elements: ['ol'] },
  rows: { elements: ['textarea'] },
  rowspan: { name: 'rowSpan', elements: ['td', 'th'] },
  sandbox: { elements: ['iframe'] },
  scope: { elements: ['th'] },
  scoped: { elements: ['style'] },
  seamless: { elements: ['iframe'] },
  selected: { elements: ['option'] },
  shape: { elements: ['a', 'area'] },
  size: { elements: ['input', 'select'] },
  sizes: { elements: ['link', 'img', 'source'] },
  span: { elements: ['col', 'colgroup'] },
  spellcheck: {},
  src: {
    elements: [
      'audio',
      'embed',
      'iframe',
      'img',
      'input',
      'script',
      'source',
      'track',
      'video',
    ],
  },
  srcdoc: { elements: ['iframe'] },
  srclang: { elements: ['track'] },
  srcset: { elements: ['img'] },
  start: { elements: ['ol'] },
  step: { elements: ['input'] },
  style: { name: 'style.cssText' },
  summary: { elements: ['table'] },
  tabindex: { name: 'tabIndex' },
  target: { elements: ['a', 'area', 'base', 'form'] },
  title: {},
  type: {
    elements: [
      'button',
      'command',
      'embed',
      'object',
      'script',
      'source',
      'style',
      'menu',
    ],
  },
  usemap: { name: 'useMap', elements: ['img', 'input', 'object'] },
  value: {
    elements: [
      'button',
      'option',
      'input',
      'li',
      'meter',
      'progress',
      'param',
      'select',
      'textarea',
    ],
  },
  volume: { elements: ['audio', 'video'] },
  playbackrate: { elements: ['audio', 'video'] },
  width: {
    elements: ['canvas', 'embed', 'iframe', 'img', 'input', 'object', 'video'],
  },
  wrap: { elements: ['textarea'] },
};

export function getAttributeName(name: string) {
  const attribute = TAG_ATTRIBUTE_LOOKUP[name.toLowerCase()];

  if (!attribute) {
    return name;
  }

  return attribute.name || name.toLowerCase();
}

// see https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes#Content_versus_IDL_attributes
export function isIdlAttribute(name: string, element: string) {
  const attribute = TAG_ATTRIBUTE_LOOKUP[name.toLowerCase()];

  return (
    Boolean(attribute) &&
    (!attribute.elements || attribute.elements.includes(element))
  );
}

export default TAG_ATTRIBUTE_LOOKUP;
