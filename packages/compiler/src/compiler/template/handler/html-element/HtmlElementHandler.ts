import { createIdentifier } from 'typescript';
import { ExpressionAttribute, ListenerAttribute, LiteralAttribute } from '../../../../ast/element-attributes';
import HtmlElement from '../../../../ast/HtmlElement';
import ElementNodeProperty from '../../../code-builder/property/ElementNodeProperty';
import { prefixExpression } from '../../../script/transform/expression-prefixer';
import { NameGenerator } from '../../NameGenerator';
import TemplateNodeVisitor from '../../TemplateNodeVisitor';
import VisitContext from '../../VisitContext';
import Handler from '../Handler';
import { getAttributeName, isIdlAttribute } from './element-tag-attribute-lookup';

export default class HtmlElementHandler implements Handler<HtmlElement> {
  constructor(private nameGenerator: NameGenerator) { }

  public build(
    node: HtmlElement,
    visitor: TemplateNodeVisitor,
    context: VisitContext,
  ): void {
    this.buildAttributes(node, context);
    this.addElement(node, context);
    this.visitChildren(node, visitor, context);
  }

  private buildAttributes(node: HtmlElement, { currentBlock, metadata }: VisitContext) {
    const name = this.nameGenerator.generateScoped(node, currentBlock);

    node.attributes.forEach(attribute => {
      if (attribute.name === 'ref') {
        if (!(attribute instanceof LiteralAttribute)) {
          throw new Error('Refs can only be literal attributes');
        }

        currentBlock.getMount().add(`this.ctx.$$component.refs[${JSON.stringify(attribute.value)}] = this.${name};`);
        currentBlock.getRemove().add(`delete this.ctx.$$component.refs[${JSON.stringify(attribute.value)}];`);

        return;
      }

      if (attribute instanceof LiteralAttribute) {
        const attributeName = getAttributeName(attribute.name);
        let expression: string;

        if (isIdlAttribute(attribute.name, node.name)) {
          expression = `this.${name}.${attributeName} = ${JSON.stringify(attribute.value)};`;
        } else {
          expression = `this.${name}.setAttribute(${JSON.stringify(attributeName)}, ${JSON.stringify(attribute.value)});`;
        }

        currentBlock
          .getMount()
          .add(expression);

        return;
      }

      if (attribute instanceof ExpressionAttribute) {
        const attributeName = getAttributeName(attribute.name);
        const prefixedExpression = prefixExpression(
          attribute.value,
          createIdentifier('ctx'),
          metadata.globals,
        );

        let expression: string;

        if (isIdlAttribute(attribute.name, node.name)) {
          expression = `this.${name}.${attributeName} = ${prefixedExpression};`;
        } else {
          expression = `this.${name}.setAttribute(${JSON.stringify(attributeName)}, ${prefixedExpression});`;
        }

        currentBlock
          .getMount()
          .add(expression);

        currentBlock
          .getUpdate()
          .add(expression);

        return;
      }

      if (attribute instanceof ListenerAttribute) {
        const prefixedExpression = prefixExpression(
          attribute.value,
          createIdentifier('this.ctx'),
          [...metadata.globals, '$$event'],
        );

        currentBlock
          .getMount()
          .add(`
            this.${name}.addEventListener("${attribute.name}", ($$event: any) => { ${prefixedExpression} });
          `);

        return;
      }
    });
  }

  private addElement(
    node: HtmlElement,
    { parent, currentBlock }: VisitContext,
  ) {
    currentBlock.addElement(
      new ElementNodeProperty(
        this.nameGenerator.generateScoped(node, currentBlock),
        node.name,
      ),
      parent && this.nameGenerator.generateScoped(parent, currentBlock),
    );
  }

  private visitChildren(
    node: HtmlElement,
    visitor: TemplateNodeVisitor,
    context: VisitContext,
  ) {
    node.children.forEach(child =>
      visitor.visit(child, { ...context, parent: node }),
    );
  }
}
