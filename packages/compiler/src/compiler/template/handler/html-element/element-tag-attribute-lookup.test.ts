import { getAttributeName, isIdlAttribute, TAG_ATTRIBUTE_LOOKUP } from './element-tag-attribute-lookup';

describe('getAttributeName', () => {
  Object.keys(TAG_ATTRIBUTE_LOOKUP).forEach(key => {
    test(key, () => {
      const lookup = TAG_ATTRIBUTE_LOOKUP[key];
      expect(getAttributeName(key)).toEqual(lookup.name || key);
    });
  });
});

describe('isIdlAttribute', () => {
  const allElements = Object.values(TAG_ATTRIBUTE_LOOKUP)
    .map(value => value.elements || [])
    .filter(value => value.length > 1)
    .reduce((previous, current) => ([...current, ...previous]), []);

  const expected: Record<string, boolean> = {};
  const actual: Record<string, boolean> = {};

  Object.keys(TAG_ATTRIBUTE_LOOKUP).forEach(tag => {
    allElements.forEach(element => {
      const lookup = TAG_ATTRIBUTE_LOOKUP[tag];
      const key = `${tag} - ${element}`;

      expected[key] = !lookup.elements || lookup.elements.includes(element);
      actual[key] = isIdlAttribute(tag, element);
    });
  });

  expect(expected).toEqual(actual);
});
