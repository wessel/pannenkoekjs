import IfStatement from '../../../ast/IfStatement';
import Block from '../../code-builder/Block';
import ClassProperty from '../../code-builder/property/ClassProperty';
import SourceFile from '../../code-builder/SourceFile';
import { DefaultNameGenerator } from '../NameGenerator';
import IfStatementHandler from './IfStatementHandler';
import { createHandlerTest, HandlerTest } from './test-utils';

describe('IfStatementHandler', () => {
  let handlerTest: HandlerTest<IfStatement>;

  beforeEach(() => {
    handlerTest = createHandlerTest(
      IfStatement,
      new IfStatementHandler(new DefaultNameGenerator()),
    );
  });

  test('without else branch', () => {
    const node = new IfStatement('false', []);

    const expectedBlock = new Block('TestBlock');
    expectedBlock.addProperty(new ClassProperty(
      'ifstatement_0',
      'IfStatementWrapper',
      undefined,
      true,
    ));
    expectedBlock.getMount().add(`
      this.ifstatement_0 = new IfStatementWrapper(
        this.slots,
        () => this.ctx,
        () => false,
        Ifstatement_0Then,
        undefined,
      );
    `);
    expectedBlock.getMount().add('target.appendChild(this.ifstatement_0.anchor);');
    expectedBlock.getMount().add('this.ifstatement_0.evaluate();');
    expectedBlock.getUpdate().add('this.ifstatement_0.evaluate();');
    expectedBlock.getRemove().add('this.ifstatement_0.remove();');

    expect(handlerTest(node)).toEqual(new SourceFile(
      [expectedBlock, new Block('Ifstatement_0Then')],
      [{ name: 'IfStatementWrapper', from: '@pannenkoekjs/runtime' }],
    ));
  });

  test('with else branch', () => {
    const node = new IfStatement('false', [], []);

    const expectedBlock = new Block('TestBlock');
    expectedBlock.addProperty(new ClassProperty(
      'ifstatement_0',
      'IfStatementWrapper',
      undefined,
      true,
    ));
    expectedBlock.getMount().add(`
      this.ifstatement_0 = new IfStatementWrapper(
        this.slots,
        () => this.ctx,
        () => false,
        Ifstatement_0Then,
        Ifstatement_0Else,
      );
    `);
    expectedBlock.getMount().add('target.appendChild(this.ifstatement_0.anchor);');
    expectedBlock.getMount().add('this.ifstatement_0.evaluate();');
    expectedBlock.getUpdate().add('this.ifstatement_0.evaluate();');
    expectedBlock.getRemove().add('this.ifstatement_0.remove();');

    expect(handlerTest(node)).toEqual(new SourceFile(
      [expectedBlock, new Block('Ifstatement_0Then'), new Block('Ifstatement_0Else')],
      [{ name: 'IfStatementWrapper', from: '@pannenkoekjs/runtime' }],
    ));
  });
});
