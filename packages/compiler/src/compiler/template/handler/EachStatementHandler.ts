import { createIdentifier } from 'typescript';
import EachStatement from '../../../ast/EachStatement';
import Block from '../../code-builder/Block';
import ClassProperty from '../../code-builder/property/ClassProperty';
import { prefixExpression } from '../../script/transform/expression-prefixer';
import { NameGenerator } from '../NameGenerator';
import TemplateNodeVisitor from '../TemplateNodeVisitor';
import VisitContext from '../VisitContext';
import Handler from './Handler';

export default class EachStatementHandler implements Handler<EachStatement> {
  constructor(private nameGenerator: NameGenerator) { }

  public build(node: EachStatement, visitor: TemplateNodeVisitor, context: VisitContext): void {
    const propertyName = this.nameGenerator.generateScoped(node, context.currentBlock);

    context.sourceFile.addRuntimeImport('EachStatementWrapper');

    context.currentBlock.addProperty(new ClassProperty(
      propertyName,
      'EachStatementWrapper',
      undefined,
      true,
    ));

    const childBlock = this.generateChildBlock(node, propertyName, visitor, context);

    context.currentBlock.getMount().add(`
      this.${propertyName} = new EachStatementWrapper(
        ${JSON.stringify(node.initializer)},
        () => this.ctx,
        ${childBlock.name},
        () => ${prefixExpression(node.expression, createIdentifier('this.ctx'))},
      );
    `);

    if (context.parent) {
      const parentName = this.nameGenerator.generateScoped(context.parent, context.currentBlock);

      context.currentBlock.getMount().add(`this.${parentName}.appendChild(this.${propertyName}.anchor);`);
    } else {
      context.currentBlock.getMount().add(`target.appendChild(this.${propertyName}.anchor);`);
    }

    context.currentBlock.getMount().add(`this.${propertyName}.evaluate();`);
    context.currentBlock.getUpdate().add(`this.${propertyName}.evaluate();`);
    context.currentBlock.getRemove().add(`this.${propertyName}.remove();`);
  }

  private generateChildBlock(
    node: EachStatement,
    name: string,
    visitor: TemplateNodeVisitor,
    context: VisitContext,
  ): Block {
    const block = new Block(this.nameGenerator.generate(node));
    context.sourceFile.addClass(block);

    const childCtx: VisitContext = { ...context, parent: undefined, currentBlock: block };
    node.children.forEach(child => visitor.visit(child, childCtx));

    return block;
  }
}
