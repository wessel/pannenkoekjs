import HtmlElement from '../../../ast/HtmlElement';
import PrintExpression from '../../../ast/PrintExpression';
import Block from '../../code-builder/Block';
import TextNodeProperty from '../../code-builder/property/TextNodeProperty';
import SourceFile from '../../code-builder/SourceFile';
import { DefaultNameGenerator } from '../NameGenerator';
import PrintExpressionHandler from './PrintExpressionHandler';
import { createHandlerTest, HandlerTest } from './test-utils';

describe('PrintExpressionHandler', () => {
  let handlerTest: HandlerTest<PrintExpression>;

  beforeEach(() => {
    handlerTest = createHandlerTest(
      PrintExpression,
      new PrintExpressionHandler(new DefaultNameGenerator()),
    );
  });

  test('simple expression', () => {
    const node = new PrintExpression('something');

    const expectedBlock = new Block('TestBlock');
    expectedBlock.addElement(new TextNodeProperty('printexpression_0'));
    expectedBlock.getMount().add('this.printexpression_0.textContent = String(ctx.something);');
    expectedBlock.getUpdate().add('this.printexpression_0.textContent = String(ctx.something);');

    expect(handlerTest(node)).toEqual(new SourceFile([expectedBlock]));
  });

  test('parent with single child', () => {
    const node = new PrintExpression('something');
    const parent = new HtmlElement('span', [], [node]);

    const expectedBlock = new Block('TestBlock');
    expectedBlock.getMount().add('this.span_0.textContent = String(ctx.something);');
    expectedBlock.getUpdate().add('this.span_0.textContent = String(ctx.something);');

    expect(handlerTest(node, parent)).toEqual(new SourceFile([expectedBlock]));
  });
});
