import ComponentElement from '../../../ast/ComponentElement';
import { ExpressionAttribute, LiteralAttribute } from '../../../ast/element-attributes';
import Block from '../../code-builder/Block';
import ClassProperty from '../../code-builder/property/ClassProperty';
import SourceFile from '../../code-builder/SourceFile';
import { ComponentScriptMetaData } from '../../script/analyzer/ComponentScriptAnalyzer';
import { DefaultNameGenerator } from '../NameGenerator';
import ComponentElementHandler from './ComponentElementHandler';
import { createHandlerTest, HandlerTest, HandlerTestNode } from './test-utils';

describe('ComponentElementHandler', () => {
  let handlerTest: HandlerTest<ComponentElement>;

  beforeEach(() => {
    handlerTest = createHandlerTest(
      ComponentElement,
      new ComponentElementHandler(new DefaultNameGenerator()),
    );
  });

  test('simple component without parent', () => {
    const node = new ComponentElement('Something');

    const expectedBlock = new Block('TestBlock');
    expectedBlock.addProperty(new ClassProperty(
      'something_0',
      'ComponentElementWrapper',
      undefined,
      true,
    ));

    expectedBlock.getMount().add(`
      this.something_0 = new ComponentElementWrapper(
        () => this.ctx,
        this.slots,
        () => Something,
        undefined,
      );
    `);
    expectedBlock.getMount().add('target.appendChild(this.something_0.anchor);');
    expectedBlock.getMount().add('this.something_0.evaluate({  });');

    expectedBlock.getUpdate().add('this.something_0.update(ctx, {  });');
    expectedBlock.getRemove().add('this.something_0.remove();');

    const expectedFile = new SourceFile([expectedBlock]);
    expectedFile.addRuntimeImport('ComponentElementWrapper');

    expect(handlerTest(node)).toEqual(expectedFile);
  });

  test('simple component with property constructor', () => {
    const node = new ComponentElement('Something');

    const expectedBlock = new Block('TestBlock');
    expectedBlock.addProperty(new ClassProperty(
      'something_0',
      'ComponentElementWrapper',
      undefined,
      true,
    ));

    expectedBlock.getMount().add(`
      this.something_0 = new ComponentElementWrapper(
        () => this.ctx,
        this.slots,
        () => ctx.Something,
        undefined,
      );
    `);
    expectedBlock.getMount().add('target.appendChild(this.something_0.anchor);');
    expectedBlock.getMount().add('this.something_0.evaluate({  });');

    expectedBlock.getUpdate().add('this.something_0.update(ctx, {  });');
    expectedBlock.getRemove().add('this.something_0.remove();');

    const metadata: ComponentScriptMetaData = {
      globals: [],
      properties: ['Something'],
    };

    const expectedFile = new SourceFile([expectedBlock]);
    expectedFile.addRuntimeImport('ComponentElementWrapper');

    expect(handlerTest(node, undefined, metadata)).toEqual(expectedFile);
  });

  test('component with attributes', () => {
    const node = new ComponentElement('Something', [
      new LiteralAttribute('literal', 'something'),
      new ExpressionAttribute('expression', 'test + 3'),
    ]);

    const expectedBlock = new Block('TestBlock');
    expectedBlock.addProperty(new ClassProperty(
      'something_0',
      'ComponentElementWrapper',
      undefined,
      true,
    ));

    expectedBlock.getMount().add(`
      this.something_0 = new ComponentElementWrapper(
        () => this.ctx,
        this.slots,
        () => Something,
        undefined,
      );
    `);
    expectedBlock.getMount().add('target.appendChild(this.something_0.anchor);');
    expectedBlock.getMount().add('this.something_0.evaluate({ literal: "something", expression: ctx.test + 3 });');

    expectedBlock.getUpdate().add('this.something_0.update(ctx, { literal: "something", expression: ctx.test + 3 });');
    expectedBlock.getUpdate().add('(this.something_0.instance! as any).expression = this.ctx.test + 3;');
    expectedBlock.getRemove().add('this.something_0.remove();');

    const expectedFile = new SourceFile([expectedBlock]);
    expectedFile.addRuntimeImport('ComponentElementWrapper');

    expect(handlerTest(node)).toEqual(expectedFile);
  });

  test('component with children', () => {
    const node = new ComponentElement('Something', [], [new HandlerTestNode('child1')]);

    const expectedBlock = new Block('TestBlock');
    expectedBlock.addProperty(new ClassProperty(
      'something_0',
      'ComponentElementWrapper',
      undefined,
      true,
    ));

    expectedBlock.getMount().add(`
      this.something_0 = new ComponentElementWrapper(
        () => this.ctx,
        this.slots,
        () => Something,
        Something_0,
      );
    `);
    expectedBlock.getMount().add('target.appendChild(this.something_0.anchor);');
    expectedBlock.getMount().add('this.something_0.evaluate({  });');

    expectedBlock.getUpdate().add('this.something_0.update(ctx, {  });');
    expectedBlock.getRemove().add('this.something_0.remove();');

    const expectedBlock2 = new Block('Something_0');

    const expectedFile = new SourceFile([expectedBlock, expectedBlock2]);
    expectedFile.addRuntimeImport('ComponentElementWrapper');

    expect(handlerTest(node)).toEqual(expectedFile);
  });
});
