import Slot from '../../../ast/Slot';
import Block from '../../code-builder/Block';
import ClassProperty from '../../code-builder/property/ClassProperty';
import SourceFile from '../../code-builder/SourceFile';
import { DefaultNameGenerator } from '../NameGenerator';
import SlotHandler from './SlotHandler';
import { createHandlerTest, HandlerTest, HandlerTestNode } from './test-utils';

describe('SlotHandler', () => {
  let handlerTest: HandlerTest<Slot>;

  beforeEach(() => {
    handlerTest = createHandlerTest(Slot, new SlotHandler(new DefaultNameGenerator()));
  });

  test('simple slot', () => {
    const node = new Slot('default');

    const expectedBlock = new Block('TestBlock');
    expectedBlock.addProperty(new ClassProperty(
      'slot_0',
      'SlotWrapper',
      undefined,
      true,
    ));
    expectedBlock.getMount().add('this.slot_0 = new SlotWrapper("default", this.slots, () => this.ctx, undefined);');
    expectedBlock.getMount().add('target.appendChild(this.slot_0.anchor);');
    expectedBlock.getMount().add('this.slot_0.mount();');
    expectedBlock.getRemove().add('this.slot_0.remove();');

    const expectedFile = new SourceFile([expectedBlock]);
    expectedFile.addRuntimeImport('SlotWrapper');

    expect(handlerTest(node)).toEqual(expectedFile);
  });

  test('slot with default', () => {
    const node = new Slot('default', [new HandlerTestNode('default')]);

    const expectedBlock = new Block('TestBlock');
    expectedBlock.addProperty(new ClassProperty(
      'slot_0',
      'SlotWrapper',
      undefined,
      true,
    ));
    expectedBlock.getMount().add('this.slot_0 = new SlotWrapper("default", this.slots, () => this.ctx, Slot_0_Default);');
    expectedBlock.getMount().add('target.appendChild(this.slot_0.anchor);');
    expectedBlock.getMount().add('this.slot_0.mount();');
    expectedBlock.getRemove().add('this.slot_0.remove();');

    const childBlock = new Block('Slot_0_Default');

    const expectedFile = new SourceFile([expectedBlock, childBlock]);
    expectedFile.addRuntimeImport('SlotWrapper');

    expect(handlerTest(node)).toEqual(expectedFile);
  });
});
