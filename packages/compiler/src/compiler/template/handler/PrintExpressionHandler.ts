import { createIdentifier } from 'typescript';
import HtmlElement from '../../../ast/HtmlElement';
import PrintExpression from '../../../ast/PrintExpression';
import TextNodeProperty from '../../code-builder/property/TextNodeProperty';
import { prefixExpression } from '../../script/transform/expression-prefixer';
import { NameGenerator } from '../NameGenerator';
import TemplateNodeVisitor from '../TemplateNodeVisitor';
import VisitContext from '../VisitContext';
import Handler from './Handler';

export default class PrintExpressionHandler implements Handler<PrintExpression> {
  constructor(private nameGenerator: NameGenerator) { }

  public build(
    node: PrintExpression,
    visitor: TemplateNodeVisitor,
    { parent, currentBlock, metadata }: VisitContext,
  ) {
    let identifier: string;
    const parentName = parent && this.nameGenerator.generateScoped(parent, currentBlock);

    if (parent instanceof HtmlElement && parent.children.length === 1) {
      identifier = parentName!;
    } else {
      identifier = this.nameGenerator.generateScoped(node, currentBlock);

      currentBlock.addElement(new TextNodeProperty(identifier), parentName);
    }

    const prefixedExpression = prefixExpression(
      node.expression,
      createIdentifier('ctx'),
      metadata.globals,
    );
    const setStatement = `this.${identifier}.textContent = String(${prefixedExpression});`;

    currentBlock.getMount().add(setStatement);
    currentBlock.getUpdate().add(setStatement);
  }
}
