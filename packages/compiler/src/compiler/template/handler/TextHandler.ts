import HtmlElement from '../../../ast/HtmlElement';
import Text from '../../../ast/Text';
import TextNodeProperty from '../../code-builder/property/TextNodeProperty';
import { NameGenerator } from '../NameGenerator';
import TemplateNodeVisitor from '../TemplateNodeVisitor';
import VisitContext from '../VisitContext';
import Handler from './Handler';

export default class TextHandler implements Handler<Text> {
  constructor(private nameGenerator: NameGenerator) { }

  public build(
    node: Text,
    visitor: TemplateNodeVisitor,
    { parent, currentBlock }: VisitContext,
  ): void {
    const name = this.nameGenerator.generateScoped(node, currentBlock);
    const parentName = parent && this.nameGenerator.generateScoped(parent, currentBlock);

    if (parent instanceof HtmlElement && parent.children.length === 1) {
      currentBlock.getMount().add(`
        this.${parentName}.textContent = ${JSON.stringify(node.value)};
      `);
    } else {
      currentBlock.addElement(
        new TextNodeProperty(name, node.value),
        parentName,
      );
    }
  }
}
