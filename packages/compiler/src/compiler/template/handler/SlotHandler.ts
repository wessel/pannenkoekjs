import Slot from '../../../ast/Slot';
import Block from '../../code-builder/Block';
import ClassProperty from '../../code-builder/property/ClassProperty';
import { NameGenerator } from '../NameGenerator';
import TemplateNodeVisitor from '../TemplateNodeVisitor';
import VisitContext from '../VisitContext';
import Handler from './Handler';

export default class SlotHandler implements Handler<Slot> {
  constructor(private nameGenerator: NameGenerator) { }

  public build(node: Slot, visitor: TemplateNodeVisitor, context: VisitContext) {
    const propertyName = this.nameGenerator.generateScoped(node, context.currentBlock);

    context.sourceFile.addRuntimeImport('SlotWrapper');
    context.currentBlock.addProperty(new ClassProperty(
      propertyName,
      'SlotWrapper',
      undefined,
      true,
    ));

    const defaultBlock = this.buildDefaultBlock(node, visitor, context);

    context.currentBlock.getMount().add(`
      this.${propertyName} = new SlotWrapper(${JSON.stringify(node.name)}, this.slots, () => this.ctx, ${defaultBlock ? defaultBlock.name : 'undefined'});`);

    if (context.parent) {
      const parentName = this.nameGenerator.generateScoped(context.parent, context.currentBlock);

      context.currentBlock.getMount().add(`this.${parentName}.appendChild(this.${propertyName}.anchor);`);
    } else {
      context.currentBlock.getMount().add(`target.appendChild(this.${propertyName}.anchor);`);
    }

    context.currentBlock.getMount().add(`this.${propertyName}.mount();`);
    context.currentBlock.getRemove().add(`this.${propertyName}.remove();`);
  }

  private buildDefaultBlock(
    node: Slot,
    visitor: TemplateNodeVisitor,
    context: VisitContext,
  ): Block | undefined {
    if (node.children.length === 0) {
      return;
    }

    const block = new Block(this.nameGenerator.generate(node, 'Default'));
    context.sourceFile.addClass(block);

    const childCtx: VisitContext = {
      ...context,
      currentBlock: block,
      parent: undefined,
    };

    node.children.forEach(child => visitor.visit(child, childCtx));

    return block;
  }
}
