import ComponentElement from '../../ast/ComponentElement';
import EachStatement from '../../ast/EachStatement';
import HtmlElement from '../../ast/HtmlElement';
import IfStatement from '../../ast/IfStatement';
import PrintExpression from '../../ast/PrintExpression';
import Slot from '../../ast/Slot';
import StyleTag from '../../ast/StyleTag';
import TemplateNode from '../../ast/TemplateNode';
import Text from '../../ast/Text';
import Block from '../code-builder/Block';

// tslint:disable-next-line: ban-types
const NODE_NAME_MAP: Map<Function, string | ((node: TemplateNode) => string)> = new Map();

NODE_NAME_MAP.set(ComponentElement, (node) => {
  const name = (node as ComponentElement).name;

  return name.charAt(0).toLowerCase() + name.slice(1);
});
NODE_NAME_MAP.set(EachStatement, 'eachstatement');
NODE_NAME_MAP.set(HtmlElement, (node) => (node as HtmlElement).name);
NODE_NAME_MAP.set(IfStatement, 'ifstatement');
NODE_NAME_MAP.set(PrintExpression, 'printexpression');
NODE_NAME_MAP.set(StyleTag, 'styletag');
NODE_NAME_MAP.set(Slot, 'slot');
NODE_NAME_MAP.set(Text, 'text');

export interface NameGenerator {
  generateScoped(node: TemplateNode, scope: Block, suffix?: string): string;
  generate(node: TemplateNode, suffix?: string): string;
}

export class NodeNameGenerator {
  // tslint:disable-next-line: ban-types
  private increments: Map<string, number> = new Map();
  private cache: Map<TemplateNode, string> = new Map();

  public generate(node: TemplateNode): string {
    if (!this.cache.has(node)) {
      const name = this.getNodeName(node);
      const increment = this.increments.get(name) || 0;

      this.increments.set(name, increment + 1);
      this.cache.set(node, `${name}_${increment}`);
    }

    return this.cache.get(node)!;
  }

  private getNodeName(node: TemplateNode): string {
    if (!NODE_NAME_MAP.has(node.constructor)) {
      throw new Error('Unknown node type');
    }

    const value = NODE_NAME_MAP.get(node.constructor)!;

    if (typeof (value) === 'string') {
      return value;
    }

    return value(node);
  }
}

export class DefaultNameGenerator implements NameGenerator {
  private defaultBlock = new Block('');
  private cache: Map<Block, NodeNameGenerator> = new Map();

  public generateScoped(node: TemplateNode, scope: Block, suffix?: string): string {
    if (!this.cache.has(scope)) {
      this.cache.set(scope, new NodeNameGenerator());
    }

    let name = this.cache.get(scope)!.generate(node);

    if (suffix) {
      name += `_${suffix}`;
    }

    return name;
  }

  public generate(node: TemplateNode, suffix?: string): string {
    const name = this.generateScoped(node, this.defaultBlock, suffix);

    return name.charAt(0).toUpperCase() + name.slice(1);
  }
}

export class ShortNameGenerator implements NameGenerator {
  private idGenerator = new StringIdGenerator();
  private nameGenerator = new DefaultNameGenerator();
  private cache: Map<string, string> = new Map();

  public generateScoped(node: TemplateNode, scope: Block, suffix?: string): string {
    return this.getShortName(this.nameGenerator.generateScoped(node, scope, suffix));
  }

  public generate(node: TemplateNode, suffix?: string): string {
    return this.getShortName(this.nameGenerator.generate(node, suffix));
  }

  private getShortName(name: string): string {
    if (!this.cache.has(name)) {
      this.cache.set(name, this.idGenerator.next());
    }

    return this.cache.get(name)!;
  }
}

class StringIdGenerator {
  private nextId = [0];

  constructor(private chars = 'abcdefghijklmnopqrstuvwxyz') { }

  public next() {
    const r = [];
    for (const char of this.nextId) {
      r.unshift(this.chars[char]);
    }
    this.increment();
    return r.join('');
  }

  public increment() {
    for (let i = 0; i < this.nextId.length; i++) {
      const val = ++this.nextId[i];
      if (val >= this.chars.length) {
        this.nextId[i] = 0;
      } else {
        return;
      }
    }
    this.nextId.push(0);
  }

  // tslint:disable-next-line: function-name
  public *[Symbol.iterator]() {
    while (true) {
      yield this.next();
    }
  }
}
