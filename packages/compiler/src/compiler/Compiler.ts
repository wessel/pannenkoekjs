import Template from '../ast/Template';
import CodeWriter from './code-builder/CodeWriter';
import SourceFile from './code-builder/SourceFile';
import { CompilationResult } from './CompilationResult';
import { CompilerOptions } from './CompilerOptions';
import ComponentScriptAnalyzer from './script/analyzer/ComponentScriptAnalyzer';
import ComponentScriptBuilder from './script/ComponentScriptBuilder';
import StyleBuilder from './style/StyleBuilder';
import TemplateBuilder from './template/TemplateBuilder';

export default class Compiler {
  private analyzer = new ComponentScriptAnalyzer();
  private scriptBuilder = new ComponentScriptBuilder();
  private styleBuilder = new StyleBuilder();
  private templateBuilder = new TemplateBuilder();

  constructor(public readonly options: CompilerOptions) { }

  public compile(template: Template): CompilationResult {
    const output = new SourceFile();
    const metadata = this.analyzer.analyze(template.script && template.script.value);

    const style = this.styleBuilder.build(template, this.options);
    this.scriptBuilder.build(template, output);
    this.templateBuilder.build(template, metadata, output, this.options);

    const writer = new CodeWriter();
    output.build(writer);

    return {
      style,
      code: writer.flush(),
    };
  }
}
