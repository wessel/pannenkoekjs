import CodeWriter from './CodeWriter';
import Method from './Method';

interface MethodTestCase {
  name: string;
  method: Method | (() => Method);
  expected: string;
}

describe('Method', () => {
  const tests: MethodTestCase[] = [
    {
      name: 'private method',
      method: new Method('test'),
      expected: `
private test() {

}
      `,
    },
    {
      name: 'public method',
      method: new Method('test', [], 'public'),
      expected: `
public test() {

}
      `,
    },
    {
      name: 'method with parameters',
      method: new Method('params', [
        { name: 'first', type: 'First' },
        { name: 'second', type: 'string' },
      ]),
      expected: `
private params(first: First, second: string) {

}
      `,
    },
    {
      name: 'method with body',
      method: () => {
        const method = new Method('withBody');
        method.add('something;');
        return method;
      },
      expected: `
private withBody() {
  something;
}
      `,
    },
  ];

  for (const { name, method, expected } of tests) {
    it(name, () => {
      const writer = new CodeWriter();

      if (typeof method === 'function') {
        method().build(writer);
      } else {
        method.build(writer);
      }

      expect(writer.flush()).toEqual(expected.trim());
    });
  }
});
