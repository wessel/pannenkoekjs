import CodeWriter from './CodeWriter';
import Method from './Method';
import ClassProperty from './property/ClassProperty';

export default class Class {
  protected properties: ClassProperty[] = [];
  protected methods: Method[] = [];

  constructor(public name: string, public extendsClass?: string) { }

  public addProperty(property: ClassProperty) {
    this.properties.push(property);
  }

  public getProperty(name: string): ClassProperty {
    const property = this.properties.find(p => p.name === name);

    if (!property) {
      throw new Error(`Property "${name}" not found`);
    }

    return property;
  }

  public addMethod(method: Method) {
    this.methods.push(method);
  }

  public getMethod(name: string): Method {
    const method = this.methods.find(m => m.name === name);

    if (!method) {
      throw new Error(`Method "${name}" not found`);
    }

    return method;
  }

  public build(writer: CodeWriter) {
    let classHeader = `class ${this.name}`;

    if (this.extendsClass) {
      classHeader += ` extends ${this.extendsClass}`;
    }

    writer.write(`${classHeader} {`);
    writer.indent();

    this.buildProperties(writer);
    this.buildMethods(writer);

    writer.outdent();
    writer.write('}');
  }

  protected buildProperties(writer: CodeWriter): void {
    this.properties.forEach(prop => prop.buildDeclaration(writer));
  }

  protected buildMethods(writer: CodeWriter): void {
    this.methods.forEach(method => method.build(writer));
  }
}
