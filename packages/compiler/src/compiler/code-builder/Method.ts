import stripIndent from 'strip-indent';
import CodeWriter from './CodeWriter';

interface MethodParameter {
  name: string;
  type: string;
}

type MethodVisibility = 'public' | 'private';

export default class Method {
  private body: string[] = [];

  public constructor(
    public name: string,
    public parameters: MethodParameter[] = [],
    public visibility: MethodVisibility = 'private',
    public overwritten: boolean = false,
  ) { }

  public add(code: string) {
    this.body.push(stripIndent(code).trim());
  }

  public prepend(code: string) {
    this.body.unshift(stripIndent(code).trim());
  }

  public build(writer: CodeWriter): void {
    const params = this.parameters
      .map(({ name, type }) => `${name}: ${type}`)
      .join(', ');

    writer.write(`${this.visibility} ${this.name}(${params}) {`);
    writer.indent();
    writer.write(this.body.join('\n'));
    writer.outdent();
    writer.write('}');
  }

  public isEmpty(): boolean {
    return this.body.length === 0;
  }
}
