import identString from 'indent-string';
import stripIdent from 'strip-indent';

export default class CodeWriter {
  private indentAmount = 0;
  private buffer: string[] = [];

  public indent(): void {
    this.indentAmount++;
  }

  public outdent(): void {
    this.indentAmount = Math.max(this.indentAmount - 1, 0);
  }

  public write(code: string): void {
    const buffer = identString(stripIdent(code), this.indentAmount, {
      indent: '  ',
    });
    this.buffer.push(buffer);
  }

  public flush(): string {
    const code = this.buffer.join('\n').trim();
    this.buffer = [];

    return code;
  }
}
