import CodeWriter from './CodeWriter';

describe('CodeWriter', () => {
  it('should strip indent', () => {
    const writer = new CodeWriter();

    writer.write(`
        public something() {
          // first rule
            // indented second rule
        }
    `);

    const expected = `
public something() {
  // first rule
    // indented second rule
}
`.trim();

    expect(writer.flush()).toEqual(expected);
  });

  it('should indent and outdent', () => {
    const writer = new CodeWriter();

    writer.write('root');
    writer.indent();
    writer.write('outer');
    writer.indent();
    writer.write('inner');
    writer.outdent();
    writer.write('outer');
    writer.outdent();
    writer.write('root');

    const expected = `
root
  outer
    inner
  outer
root
`.trim();

    expect(writer.flush()).toEqual(expected);
  });

  it('should reset the internal buffer after a flush', () => {
    const writer = new CodeWriter();

    writer.write('something');
    expect(writer.flush()).toEqual('something');

    writer.write('something else');
    expect(writer.flush()).toEqual('something else');

    expect(writer.flush()).toEqual('');
  });
});
