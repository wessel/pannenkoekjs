import ClassProperty from './ClassProperty';

export default class TextNodeProperty extends ClassProperty {
  constructor(name: string, value?: string) {
    super(name, 'Text', `document.createTextNode(${JSON.stringify(value || '')})`);
  }
}
