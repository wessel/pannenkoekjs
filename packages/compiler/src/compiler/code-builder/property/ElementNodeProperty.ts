import ClassProperty from './ClassProperty';

export default class ElementNodeProperty extends ClassProperty {
  constructor(name: string, type: string) {
    super(
      name,
      `HTMLElementTagNameMap[${JSON.stringify(type)}]`,
      `document.createElement(${JSON.stringify(type)})`,
    );
  }
}
