import CodeWriter from '../CodeWriter';

export default class ClassProperty {
  constructor(
    public name: string,
    public type: string,
    public initializer?: string,
    public withExclamationMark?: boolean,
  ) { }

  public buildDeclaration(writer: CodeWriter): void {
    const exclamationMark = this.withExclamationMark ? '!' : '';
    let declaration = `private ${this.name}${exclamationMark}: ${this.type}`;

    if (this.initializer) {
      declaration += ` = ${this.initializer}`;
    }

    declaration += ';';

    writer.write(declaration);
  }

  public buildInitializer(writer: CodeWriter): void {
    if (!this.initializer || this.initializer.length === 0) {
      return;
    }

    writer.write(`this.${this.name} = ${this.initializer};`);
  }
}
