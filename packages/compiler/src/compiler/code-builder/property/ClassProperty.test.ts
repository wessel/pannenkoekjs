import CodeWriter from '../CodeWriter';
import ClassProperty from './ClassProperty';

describe('ClassProperty', () => {
  it('should build the declaration', () => {
    const writer = new CodeWriter();
    const property = new ClassProperty('prop', 'HTMLElement');
    property.buildDeclaration(writer);

    expect(writer.flush()).toEqual('private prop: HTMLElement;');
  });

  it('should build the initializer', () => {
    const writer = new CodeWriter();
    const property = new ClassProperty('prop', 'HTMLElement', 'true');
    property.buildInitializer(writer);

    expect(writer.flush()).toEqual('this.prop = true;');
  });
});
