import Class from './Class';
import CodeWriter from './CodeWriter';
import Method from './Method';
import ClassProperty from './property/ClassProperty';

interface ClassTestCase {
  name: string;
  c: Class | (() => Class);
  expected: string;
}

describe('Class', () => {
  const tests: ClassTestCase[] = [
    {
      name: 'empty class',
      c: new Class('Test'),
      expected: `
class Test {
}
      `,
    },
    {
      name: 'class with properties',
      c: () => {
        const c = new Class('WithProperties');
        c.addProperty(new ClassProperty('test1', 'Test'));
        c.addProperty(new ClassProperty('test2', 'boolean'));

        return c;
      },
      expected: `
class WithProperties {
  private test1: Test;
  private test2: boolean;
}
`,
    },
    {
      name: 'class with methods',
      c: () => {
        const c = new Class('WithMethods');
        c.addMethod(new Method('test'));
        c.addMethod(
          new Method('otherTest', [{ name: 'test', type: 'any' }], 'public'),
        );
        return c;
      },
      expected: `
class WithMethods {
  private test() {

  }
  public otherTest(test: any) {

  }
}
      `,
    },
  ];

  for (const { name, c, expected } of tests) {
    it(name, () => {
      const writer = new CodeWriter();

      if (typeof c === 'function') {
        c().build(writer);
      } else {
        c.build(writer);
      }

      expect(writer.flush()).toEqual(expected.trim());
    });
  }
});
