import stripIndent from 'strip-indent';
import Class from './Class';
import CodeWriter from './CodeWriter';

export interface Import {
  name: string;
  from: string;
}

export default class SourceFile {
  private body: string[] = [];

  constructor(private classes: Class[] = [], private imports: Import[] = []) { }

  public addCode(code: string) {
    this.body.push(stripIndent(code).trim());
  }

  public addClass(definition: Class) {
    this.classes.push(definition);
  }

  public addImport(definition: Import) {
    const existing = this.imports.find(
      def => def.name === definition.name && def.from === definition.from,
    );

    if (existing) {
      return;
    }

    this.imports.push(definition);
  }

  public addRuntimeImport(name: string) {
    this.addImport({ name, from: '@pannenkoekjs/runtime' });
  }

  public build(writer: CodeWriter) {
    this.buildImports(writer);

    writer.write(this.body.join('\n'));

    this.classes.forEach(definition => definition.build(writer));
  }

  private buildImports(writer: CodeWriter) {
    const map: Record<string, string[]> = {};

    this.imports.forEach(definition => {
      const previous: string[] = map[definition.from] || [];

      map[definition.from] = [...previous, definition.name];
    });

    for (const from of Object.keys(map)) {
      writer.write(`import { ${map[from].join(', ')} } from "${from}"`);
    }
  }
}
