import Class from './Class';
import CodeWriter from './CodeWriter';
import Method from './Method';
import ClassProperty from './property/ClassProperty';

export default class Block extends Class {
  constructor(name: string) {
    super(name);

    this.addMethod(new Method(
      'mount',
      [
        { name: 'target', type: 'any' },
        { name: 'slots', type: 'any' },
        { name: 'ctx', type: 'any' },
      ],
      'public',
      true,
    ));
    this.addMethod(new Method('update', [{ name: 'ctx', type: 'any' }], 'public', true));
    this.addMethod(new Method('remove', [], 'public', true));

    this.extendsClass = 'Block';
  }

  public getMount(): Method {
    return this.getMethod('mount');
  }

  public getUpdate(): Method {
    return this.getMethod('update');
  }

  public getRemove(): Method {
    return this.getMethod('remove');
  }

  public addElement(property: ClassProperty, parentName?: string) {
    this.addProperty(property);

    if (parentName) {
      this.getMount().add(
        `this.${parentName}.appendChild(this.${property.name});`,
      );
    } else {
      this.getMount().add(`target.appendChild(this.${property.name});`);
      this.getRemove().add(`this.${property.name}.remove();`);
    }
  }

  protected buildMethods(writer: CodeWriter): void {
    this.methods.forEach(method => {
      if (method.overwritten) {
        if (method.isEmpty()) {
          return;
        }

        const args = method.parameters.map(param => param.name).join(', ');
        method.prepend(`super.${method.name}(${args});`);
      }

      method.build(writer);
    });
  }
}
