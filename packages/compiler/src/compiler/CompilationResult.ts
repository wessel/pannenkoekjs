export interface CompilationResult {
  code: string;
  style?: string;
}
