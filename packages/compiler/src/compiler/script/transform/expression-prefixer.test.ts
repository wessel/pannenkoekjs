import { createIdentifier } from 'typescript';
import { prefixExpression } from './expression-prefixer';

type TableTests = Record<string, string>;

const tests: TableTests = {
  something: 'ctx.something',
  '1337 || "test"': '1337 || "test"',
  'something && something[different] || something.different': 'ctx.something && ctx.something[ctx.different] || ctx.something.different',
};

describe('expression prefixer', () => {
  Object.keys(tests).forEach(key => {
    it(key, () => {
      expect(prefixExpression(key, createIdentifier('ctx'))).toEqual(tests[key]);
    });
  });
});
