import { ClassDeclaration, createBlock, createConstructor, createIdentifier, createMethod, createModifier, createNodeArray, createObjectLiteral, createParameter, createProperty, createTypeReferenceNode, getMutableClone, isComputedPropertyName, isConstructorDeclaration, isMethodDeclaration, isPropertyDeclaration, PropertyName, SyntaxKind } from 'typescript';
import { parseCode } from '../utils/parser';

export default class ComponentClassTransformer {
  public transform(declaration: ClassDeclaration, className: string): ClassDeclaration {
    const transformed = getMutableClone(declaration);

    this.changeClassName(transformed, className);
    const hasOldConstructor = this.moveConstructor(transformed);
    this.createConstructor(transformed, hasOldConstructor);
    this.addComponentProperty(transformed);

    return transformed;
  }

  private addComponentProperty(declaration: ClassDeclaration) {
    declaration.members = createNodeArray([
      createProperty(
        undefined,
        [createModifier(SyntaxKind.PublicKeyword)],
        '$$component',
        undefined,
        createTypeReferenceNode('Component', undefined),
        undefined,
      ),
      ...declaration.members,
    ]);
  }

  private moveConstructor(declaration: ClassDeclaration): boolean {
    const construct = declaration.members.find(isConstructorDeclaration);

    if (!construct) {
      return false;
    }

    // todo: what about parameters?

    const method = createMethod(
      undefined,
      [createModifier(SyntaxKind.PrivateKeyword)],
      undefined,
      '$$originalConstructor',
      undefined,
      undefined,
      [],
      undefined,
      construct.body,
    );

    declaration.members = createNodeArray(
      [
        ...declaration.members.filter(member => member !== construct),
        method,
      ],
    );

    return true;
  }

  private createConstructor(declaration: ClassDeclaration, hasOldConstructor: boolean) {
    const construct = createConstructor(
      undefined,
      undefined,
      createNodeArray([
        createParameter(
          undefined,
          undefined,
          undefined,
          'props',
          undefined,
          createTypeReferenceNode(createIdentifier('any'), undefined),
          createObjectLiteral(),
        ),
      ]),
      createBlock(this.createConstructorCode(declaration, hasOldConstructor), true),
    );

    declaration.members = createNodeArray([construct, ...declaration.members]);
  }

  private createConstructorCode(declaration: ClassDeclaration, hasOldConstructor: boolean) {
    const methodBindings: string = declaration.members
      .filter(isMethodDeclaration)
      .filter(method => !this.getPropertyName(method.name).startsWith('$$'))
      .map(method => {
        const name = this.getPropertyName(method.name);

        return `this.${name} = this.${name}.bind(proxy);`;
      })
      .join('\n');

    const propertySetters = declaration.members
      .filter(isPropertyDeclaration)
      .map(prop => {
        if (!prop.modifiers || !prop.modifiers.find(mod => mod.kind === SyntaxKind.PublicKeyword)) {
          return;
        }

        const name = this.getPropertyName(prop.name);

        return `this.${name} = props.${name};`;
      })
      .filter((value): value is string => !!value)
      .join('\n');

    return parseCode(`
      ${propertySetters}

      const proxy = createProxy(this);
      this.$$component = new Component(proxy, MainBlock);

      ${methodBindings}
      ${hasOldConstructor ? 'proxy.$$originalConstructor()' : ''}

      return proxy;
    `);
  }

  private changeClassName(declaration: ClassDeclaration, className: string): void {
    declaration.name = createIdentifier(className);
  }

  private getPropertyName(name: PropertyName): string {
    if (isComputedPropertyName(name)) {
      throw new Error("Don't know how to get the computed property name");
    }

    return name.text;
  }
}
