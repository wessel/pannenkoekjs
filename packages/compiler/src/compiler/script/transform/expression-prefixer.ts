import { createPropertyAccess, Expression, ExpressionStatement, getMutableClone, isElementAccessExpression, isExpressionStatement, isIdentifier, isPropertyAccessExpression, Node, transform, TransformationContext, visitEachChild } from 'typescript';
import { parseCode } from '../utils/parser';
import { printNode } from '../utils/printer';

export const prefixExpression = (
  original: string,
  prefix: Expression,
  exclude: string[] = [],
): string => {
  const expression = parseExpression(original);
  const visit = visitExpression(prefix, exclude);

  const transformed = transform<Expression>(expression, [
    (context) => (node) => visit(node, context),
  ]);

  if (transformed.transformed.length !== 1) {
    throw new Error('invalid transform');
  }

  return printNode(transformed.transformed[0]);
};

const parseExpression = (expression: string): Expression => {
  const parsed = parseCode(expression);

  if (parsed.length !== 1 || !isExpressionStatement(parsed[0])) {
    throw new Error('Invalid expression given');
  }

  const statement = parsed[0] as ExpressionStatement;

  return statement.expression;
};

const visitExpression = (prefix: Expression, exclude: string[]) =>
  <T extends Node>(
    node: T,
    context: TransformationContext,
  ): T => {
    const visit = visitExpression(prefix, exclude);

    if (isPropertyAccessExpression(node)) {
      const propAccess = getMutableClone(node);
      propAccess.expression = visit(node.expression, context);

      return propAccess;
    }

    if (isElementAccessExpression(node)) {
      const elemAccess = getMutableClone(node);
      elemAccess.expression = visit(node.expression, context);
      elemAccess.argumentExpression = visit(node.argumentExpression, context);

      return elemAccess;
    }

    if (isIdentifier(node) && !exclude.includes(node.text)) {
      return createPropertyAccess(prefix, node) as any;
    }

    return visitEachChild(node, visit as any, context);
  };
