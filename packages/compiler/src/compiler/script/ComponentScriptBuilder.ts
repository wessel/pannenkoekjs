import { ClassDeclaration, isClassDeclaration, Modifier, NodeArray, Statement as TsStatement, SyntaxKind } from 'typescript';
import Template from '../../ast/Template';
import SourceFile from '../code-builder/SourceFile';
import ComponentClassTransformer from './transform/ComponentClassTransformer';
import { parseCode } from './utils/parser';
import { printNode, printNodes } from './utils/printer';

export default class ComponentScriptBuilder {
  public build(template: Template, output: SourceFile) {
    output.addRuntimeImport('Component');
    output.addRuntimeImport('Block');
    output.addRuntimeImport('createProxy');

    const source: TsStatement[] = template.script ? parseCode(template.script.value) : [];
    const [statements, componentClass] = this.findComponentClass(source);

    output.addCode(printNodes(statements));

    if (!componentClass) {
      output.addCode(this.generateComponentClass(template.name));
      return;
    }

    output.addCode(printNode(
      this.transformComponentClass(componentClass, template.name),
    ));
  }

  private findComponentClass(statements: TsStatement[]): [
    TsStatement[],
    ClassDeclaration | undefined
  ] {
    const hasModifier = (kind: SyntaxKind, modifiers?: NodeArray<Modifier>): boolean =>
      (modifiers && modifiers.find(modifier => modifier.kind === kind)) !== undefined;

    const found = statements.find(
      (statement): statement is ClassDeclaration => (
        isClassDeclaration(statement)
        && hasModifier(SyntaxKind.ExportKeyword, statement.modifiers)
        && hasModifier(SyntaxKind.DefaultKeyword, statement.modifiers)
      ),
    );

    return [
      statements.filter(statement => statement !== found),
      found,
    ];
  }

  private generateComponentClass(name: string): string {
    return `
      export default class ${name} {
        public $$component: Component = new Component(this, MainBlock);
        constructor(props: any = {}) { }
      }
    `;
  }

  private transformComponentClass(declaration: ClassDeclaration, name: string): ClassDeclaration {
    const transformer = new ComponentClassTransformer();

    return transformer.transform(declaration, name);
  }
}
