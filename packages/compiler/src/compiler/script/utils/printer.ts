import { createNodeArray, createPrinter, createSourceFile, EmitHint, ListFormat, Node, ScriptTarget } from 'typescript';

const printer = createPrinter();
const file = createSourceFile('', '', ScriptTarget.Latest);

export const printNode = (node: Node) => printer.printNode(
  EmitHint.Unspecified,
  node,
  file,
);

export const printNodes = (nodes: Node[]) =>
  printer.printList(ListFormat.MultiLine, createNodeArray(nodes), file);
