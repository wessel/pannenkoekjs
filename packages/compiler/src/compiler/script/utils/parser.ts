import { createSourceFile, Expression, ExpressionStatement, isExpressionStatement, ScriptTarget, Statement } from 'typescript';

export const parseCode = (code: string): Statement[] => {
  const file = createSourceFile('', code, ScriptTarget.Latest);

  return [...file.statements];
};

export const parseExpression = (expression: string): Expression => {
  const parsed = parseCode(expression);

  if (parsed.length !== 1 || !isExpressionStatement(parsed[0])) {
    throw new Error('Invalid expression given');
  }

  const statement = parsed[0] as ExpressionStatement;

  return statement.expression;
};
