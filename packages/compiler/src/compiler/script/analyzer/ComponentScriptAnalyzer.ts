import { ClassDeclaration, createSourceFile, isClassDeclaration, isComputedPropertyName, isIdentifier, isImportDeclaration, isNamedImports, isPropertyDeclaration, isVariableStatement, NodeArray, ScriptTarget, Statement } from 'typescript';

export interface ComponentScriptMetaData {
  globals: string[];
  properties: string[];
}

export default class ComponentScriptAnalyzer {
  public analyze(source?: string): ComponentScriptMetaData {
    if (!source || source.trim().length === 0) {
      return {
        globals: [],
        properties: [],
      };
    }

    const sourceFile = createSourceFile('', source, ScriptTarget.Latest);

    return {
      globals: this.findGlobals(sourceFile.statements),
      properties: this.findProperties(sourceFile.statements),
    };
  }

  private findGlobals(statements: NodeArray<Statement>): string[] {
    const globals: string[] = [];

    statements.forEach(statement => {
      // todo: check all global types

      if (isImportDeclaration(statement) && statement.importClause) {
        if (statement.importClause.name) {
          globals.push(statement.importClause.name.text);
        }

        if (statement.importClause.namedBindings
          && isNamedImports(statement.importClause.namedBindings)) {
          statement.importClause.namedBindings.elements.forEach(specifier => {
            globals.push(specifier.name.text);
          });
        }
      }

      if (isVariableStatement(statement)) {
        statement.declarationList.declarations.forEach(declaration => {
          if (isIdentifier(declaration.name)) {
            globals.push(declaration.name.text);
          } else {
            throw new Error('Unsupported!');
          }
        });
      }
    });

    return globals;
  }

  private findProperties(statements: NodeArray<Statement>): string[] {
    const exportedClass = this.findDefaultExportedClass(statements);

    if (!exportedClass) {
      return [];
    }

    const properties: string[] = [];

    exportedClass.members.forEach(member => {
      if (isPropertyDeclaration(member)) {
        if (!isComputedPropertyName(member.name)) {
          properties.push(member.name.text);

        }
      }
    });

    return properties;
  }

  private findDefaultExportedClass(statements: NodeArray<Statement>): ClassDeclaration | undefined {
    return statements.find(statement => isClassDeclaration(statement)) as ClassDeclaration;
  }
}
