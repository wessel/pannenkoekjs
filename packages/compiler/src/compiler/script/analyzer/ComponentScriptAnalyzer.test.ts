import ComponentScriptAnalyzer, { ComponentScriptMetaData } from './ComponentScriptAnalyzer';

describe('ComponentScriptAnalyzer', () => {
  it('', () => {
    const analyzer = new ComponentScriptAnalyzer();

    const result = analyzer.analyze(`
      import something from 'test';
      import { destructuredTest1, destructuredTest2 } from 'something';

      export default class {
        private prop1 = '';
        public prop2 = 1337;
      }
    `);

    const expected: ComponentScriptMetaData = {
      globals: ['something', 'destructuredTest1', 'destructuredTest2'],
      properties: ['prop1', 'prop2'],
    };

    expect(result).toEqual(expected);
  });
});
