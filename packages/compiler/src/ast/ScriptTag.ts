import TemplateNode from './TemplateNode';

export default class ScriptTag implements TemplateNode {
  constructor(public value: string) { }
}
