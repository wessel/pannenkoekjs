import TemplateNode from './TemplateNode';

export default class EachStatement implements TemplateNode {
  constructor(
    public initializer: string,
    public expression: string,
    public children: TemplateNode[],
    public indexIdentifier?: string,
  ) { }
}
