import TemplateNode from './TemplateNode';

export default class PrintExpression implements TemplateNode {
  constructor(public expression: string) { }
}
