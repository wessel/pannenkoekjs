import ComponentElement from './ComponentElement';
import EachStatement from './EachStatement';
import HtmlElement from './HtmlElement';
import IfStatement from './IfStatement';
import PrintExpression from './PrintExpression';
import ScriptTag from './ScriptTag';
import Slot from './Slot';
import StyleTag from './StyleTag';
import Text from './Text';

export default interface TemplateNode {
}

export type TemplateNodeType =
  | ComponentElement
  | EachStatement
  | HtmlElement
  | IfStatement
  | PrintExpression
  | ScriptTag
  | StyleTag
  | Slot
  | Text;
