import { ComponentAttribute } from './element-attributes';
import TemplateNode from './TemplateNode';

export default class ComponentElement implements TemplateNode {
  constructor(
    public name: string,
    public attributes: ComponentAttribute[] = [],
    public children: TemplateNode[] = [],
  ) { }
}
