import TemplateNode from './TemplateNode';

export default class StyleTag implements TemplateNode {
  constructor(public value: string) { }
}
