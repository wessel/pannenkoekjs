import ScriptTag from './ScriptTag';
import StyleTag from './StyleTag';
import TemplateNode from './TemplateNode';

export default class Template {
  constructor(
    public name: string,
    public nodes: TemplateNode[],
    public script?: ScriptTag,
    public style?: StyleTag,
  ) { }
}
