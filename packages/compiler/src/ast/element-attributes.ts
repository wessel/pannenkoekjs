export class LiteralAttribute {
  constructor(public name: string, public value: string) { }
}

export class ExpressionAttribute {
  constructor(public name: string, public value: string) { }
}

export class ListenerAttribute {
  constructor(public name: string, public value: string) { }
}

export type ElementAttribute = LiteralAttribute | ExpressionAttribute | ListenerAttribute;

export type ComponentAttribute = LiteralAttribute | ExpressionAttribute;
