import { ElementAttribute } from './element-attributes';
import TemplateNode from './TemplateNode';

export default class HtmlElement implements TemplateNode {
  constructor(
    public name: string,
    public attributes: ElementAttribute[] = [],
    public children: TemplateNode[] = [],
  ) { }
}
