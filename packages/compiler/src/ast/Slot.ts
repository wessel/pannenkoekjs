import TemplateNode from './TemplateNode';

export default class Slot implements TemplateNode {
  constructor(public name: string, public children: TemplateNode[] = []) { }
}
