import TemplateNode from './TemplateNode';

export default class Text implements TemplateNode {
  constructor(public value: string) { }
}
