import TemplateNode from './TemplateNode';

export default class IfStatement implements TemplateNode {
  constructor(
    public condition: string,
    public thenBranch: TemplateNode[],
    public elseBranch?: TemplateNode[],
  ) { }
}
